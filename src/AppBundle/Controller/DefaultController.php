<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\File\File;

use AppBundle\Model\NhansuQuery;
use AppBundle\Model\AdminQuery;
use AppBundle\Model\Admin;


class DefaultController extends Controller
{
    /**
     * @Route("/index", name="index")
     */
    public function indexAction(Request $request)
    {

        $session = new Session();
        $page = $request->query->get('page', 1);
        $limit  = 10;
        $user = NhansuQuery::create()->filterByState(true);
        $user_pagi = $user->paginate($page, $limit);
        $notify=$session->getFlashBag()->get('notify');
        if( $notify){
      
          return $this->render('@App/index.html.twig',['user_pagi' => $user_pagi,
            'notify'=> $notify[0] ,'acc'=>$session->get('account'),'img'=>$session->get('img')]);
        }
        if($session->get('account')==null){
          return $this->render('@App/warning.html.twig');
        }

        
        return $this->render('@App/index.html.twig',['user_pagi' => $user_pagi,'acc'=>$session->get('account'),'img'=>$session->get('img'),'page' => $page]);
    }
    /**
     * @Route("/notify", name="notify")
     */
    public function notifyAction(Request $request)
    {
        $session = new Session();
        $session->remove('notify');
        $page = $request->query->get('page', 1);
        $limit  = 10;
        $user = NhansuQuery::create()->filterByState(true);
        $user_pagi = $user->paginate($page, $limit);

        return $this->render('@App/templates/dsns.html.twig',['user_pagi' => $user_pagi,'page' => $page]);
         
    }
    /**
     * @Route("/dsns", name="dsns")
     */
    public function viewListAction(Request $request)
    {  
        $page = $request->query->get('page', 1);
       if($request->query->get('page')){
         $page = $request->query->get('page');
       }
        $limit  = 10;
        $user = NhansuQuery::create()->filterByState(true);
        $user_pagi = $user->paginate($page, $limit);

        return $this->render('@App/templates/dsns.html.twig',['user_pagi' => $user_pagi,'page' => $page]);
         
    }
    /**
     * @Route("/", name="login")
     */
    public function loginAction(Request $request)
    {   
       $session = new Session();
         if ($request->isMethod('POST')) {
           
            $errors=array();
            if($request->get('account')==null){
                $errors['account']='Hãy nhập tài khoản của bạn ! ';
            }
            $acc=trim($request->get('account'));
            $user= AdminQuery::create()->filterByAccount($acc)->findOne();
 
            if($user==null)
                 $errors['account']='Bạn đã nhập sai tài khoản !';

            if($user!=null && $user->getPassword()!= $request->get('password'))
                $errors['password']='Bạn đã nhập sai mật khẩu !';
            if($request->get('password')== '')
                $errors['password']='Bạn chưa nhập mật khẩu !';
            if(!$errors){
                $session->set('account', $user->getAccount());
                $session->set('img', $user->getImages());

                return $this->redirectToRoute('index'); 
            }
           
            else{
                return $this->render('@App/login.html.twig',['acc'=>$acc,'errors'=>$errors]);
           }
   }
        if( $session->get('account') && $session->get('img')){
            return $this->redirectToRoute('index'); 
             
        }
        if($session->get('notiAcc'))
            return $this->render('@App/login.html.twig',['notiAcc'=>$session->get('notiAcc')]);
         return $this->render('@App/login.html.twig');
         
    }
    /**
     * @Route("/register", name="register")
     */
    public function registerAction(Request $request)
    {      
        if ($request->isMethod('POST')) {
          $files = $request->files->get('image');
          
          $user = new Admin();
          $errors=array();
            if($request->get('account')=='')
                 $errors['account']='Bạn không được để trống tài khoản ! ';
            $check_account= AdminQuery::create()->filterByAccount($request->get('account'))->findOne();
            if ($check_account != null) {
                $errors['account'] = "Tên tài khoản đã được sử dụng !";
            }
            if (!preg_match("/^[a-zA-Z0-9_ ]*$/",$request->get('account'))) {
                $errors['account']= 'Tài khoản chứa ký tự không hợp lệ !';
            }
            if(strlen($request->get('account'))<5 || strlen($request->get('account'))>20){
                $errors['account']= 'Tài khoản có độ dài không hợp lệ !';  
            }
            if(strlen($request->get('password'))<5 ){
                $errors['password']='Mật khẩu không hợp lệ !';        
            }
            if($request->get('confirmpassword')=='')
                 $errors['confirmpassword']='Phải nhập lại mật khẩu ! ';
            if($request->get('confirmpassword')!=$request->get('password'))
                 $errors['confirmpassword']='Mật khẩu nhập lại không khớp ! ';
            if($request->get('email')=='')
                 $errors['email']='Không được để trống email ! ';
             $check_email= AdminQuery::create()->filterByEmail($request->get('email'))->findOne();
            if ($check_email != null) {
                $errors['email'] = "Email đã được sử dụng !";
            }
            if($request->get('password')=='')
                 $errors['password']='Không được để trống mật khẩu ! ';
            if($request->files->get('image') == null)
                 $errors['image']='Không được để trống ảnh ! ';
            if(!$errors){
                $user->setAccount($request->get('account')!=null ? trim($request->get('account')):'');
                $user->setEmail($request->get('email') !=null ? trim($request->get('email')) :'');
                $user->setPassword($request->get('password')!=null?$request->get('password'):'');
                $user->setImages($files != null ? $files->getClientOriginalName() : 'default.png');
                $user->save();
                move_uploaded_file($files->getPathName(), 'userfiles/'.$files->getClientOriginalName());
                $session=new Session;
                $session->set('account', $user->getAccount());
                $session->set('img', $user->getImages());
                

                return $this->redirectToRoute('index'); 
            }
            else{
                return $this->render('@App/register.html.twig',['errors'=>$errors,'email' => $request->get('email'), 'acc' => $request->get('account')]);

            }
            
      }
        return $this->render('@App/register.html.twig');
         
    }
     /**
     * @Route("/logout", name="logout")
     */
    public function logoutAction(Request $request)
    {   
        $session=new Session;
        $session->invalidate();
        return $this->redirectToRoute('login'); 
    }
      /**
     * @Route("/forgetpass", name="forgetpass")
     */
    public function forgetpassAction(Request $request)
    {       
        if ($request->isMethod('POST')) {
        
          $errors=array();
          $user= AdminQuery::create()->filterByAccount($request->get('account'))->findOne();
            if($request->get('account')=='')
                 $errors['account']='Bạn không được để trống tài khoản ! ';
          
            if ($user != null) {
                if($user->getEmail()!=$request->get('email'))
                  $errors['email']='Bạn nhập email không chính xác ! ';  
                
            }
            if ($user == null ) {
                     $errors['email']='Bạn cần nhập tài khoản và email ! ';
            }
        
            if(strlen($request->get('password'))<5 ){
                $errors['password']='Mật khẩu không hợp lệ !';        
            }
            if($request->get('email')=='')
                 $errors['email']='Bạn không được để trống email ! ';
            if($request->get('password')=='')
                 $errors['password']='Bạn không được để trống mật khẩu ! ';
            if($request->get('confirmpassword')=='')
                 $errors['confirmpassword']='Bạn phải nhập lại mật khẩu ! ';
            if($request->get('confirmpassword')!=$request->get('password'))
                 $errors['confirmpassword']='Mật khẩu nhập lại không khớp ! ';
           
            if(!$errors){
               
                $user->setPassword($request->get('password')!=null?$request->get('password'):'');
        
              
                $user->save();
                $session=new Session;
                $session->set('account', $user->getAccount());
                $session->set('img', $user->getImages());

                return $this->redirectToRoute('index'); 
            }
            else{

                return $this->render('@App/forgetpass.html.twig',['email'=>$request->get('email'),'account'=>$request->get('account'),'errors'=>$errors]);

            }
            
      }
        return $this->render('@App/forgetpass.html.twig');
         
    }
    /**
     * @Route("/forgetacc", name="forgetacc")
    */
    public function forgetaccAction(Request $request)
    {       
        if ($request->isMethod('POST')) {
        
          $errors=array();
          $user= AdminQuery::create()->filterByEmail($request->get('email'))->findOne();
          
            if($request->get('email')=='')
                 $errors['email']='Bạn không được để trống email ! ';
            if($request->get('password')=='')
                 $errors['password']='Bạn không được để trống mật khẩu ! ';
            if($user!=null){
                if($user->getPassword()!=$request->get('password'))
                     $errors['password']='Mật khẩu không đúng ! ';
            }
            if($request->get('check')=='')
                 $errors['check']='Bạn phải xác nhận cấp lại tài khoản ! ';
            if($user==null)
                 $errors['password']='Bạn cần nhập email và mật khẩu  ! ';
            if(!$errors){
               
               $session= new Session;
               $session->set('notiAcc',' Tên Tài khoản đã được gửi tới email của bạn !');

                return $this->redirectToRoute('login');
            }
            else{
                return $this->render('@App/forgetacc.html.twig',['email'=>$request->get('email'),'errors'=>$errors]);

            }
            
      }
        return $this->render('@App/forgetacc.html.twig');
         
    }
    /**
     * @Route("/notiAcc", name="notiAcc")
    */
    public function notiAccAction(Request $request)
    { 
        $session = new Session();
        $session->remove('notiAcc');
        return $this->render('@App/templates/loginform.html.twig');
    }  
    /**
     * @Route("/search", name="search")
    */
    public function searchAction(Request $request)
    { 
        $key = $request->query->get('name');
        $hoten=explode(' ', $key);
        $user  = NhansuQuery::create()
        ->where('nhansu.ho like ?','%'.$key.'%')
        ->_or()
        ->where('nhansu.ten like ?','%'.$key.'%')
        ->_or()
        ->where('nhansu.ma_nv like ?','%'.$key.'%')
        ->_or()
        ->where('nhansu.hovaten like ?','%'.$key.'%');
        $page = $request->query->get('page', 1);
        $limit  = 10;
        $user_pagi = $user->paginate($page, $limit);
        
        $e=$user->find();
        $error=1;
        if(empty($e->toArray())){
          $error=0;
        }
        return $this->render('@App/templates/dsns.html.twig',['user_pagi' => $user_pagi,'page' => $page,'error'=>$error]);
    
  
    }
 
    
}

