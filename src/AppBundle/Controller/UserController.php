<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\File\File;
use AppBundle\Model\NhansuQuery;
use AppBundle\Model\Nhansu;

class UserController extends Controller
{

    /**
     * @Route("/user/add", name="user_add")
     */
    public function addUserAction(Request $request){
    	
        $userdesc = NhansuQuery::create()->orderById('desc')->findOne();
        $ma=1+$userdesc->getId();
        $manv= 'NV'.$ma;
        $session=new Session;
    	if ($request->isMethod('POST')) {
                $files = $request->files->get('anh');
                $user = new Nhansu();
	    		$errors = array();
	    		$user->setMaNv($manv);

	    		$user->setHo(($request->get('ho')!=null)?$request->get('ho'):'');
	    		$user->setTen(($request->get('ten')!=null)?$request->get('ten'):'');
	    		$user->setGioitinh(($request->get('gioitinh')!=null)?$request->get('gioitinh'):'');
	    		$user->setNgaysinh(($request->get('ngaysinh')!=null)?$request->get('ngaysinh'):'');
	    		$user->setQuoctich(($request->get('quoctich')!=null)?$request->get('quoctich'):'');
	    		$user->setDantoc(($request->get('dantoc')!=null)?$request->get('dantoc'):'');
	    		$user->setTongiao(($request->get('tongiao')!=null)?$request->get('tongiao'):'');
	    		$user->setSoCmnd(($request->get('soCMND')!=null)?$request->get('soCMND'):'');
	    		$user->setNgaycap(($request->get('ngaycap')!=null)?$request->get('ngaycap'):'');
	    		$user->setNoicap(($request->get('noicap')!=null)?$request->get('noicap'):'');
	    		
                $user->setAnh($files != null ? $files->getClientOriginalName() : 'default.png');
         
               
	    		$user->setDidong(($request->get('didong')!=null)?$request->get('didong'):'');
	    		$user->setEmail(($request->get('email')!=null)?$request->get('email'):'');
	    		$user->setSkype(($request->get('skype')!=null)?$request->get('skype'):'');
	    		$user->setFacebook(($request->get('facebook')!=null)?$request->get('facebook'):'');
	    		$user->setQuocgia(($request->get('quocgia')!='Chọn')?$request->get('quocgia'):'');
	    		$user->setTinhthanh(($request->get('tinhthanh')!='Chọn')?$request->get('tinhthanh'):'');
	    		$user->setQuanhuyen(($request->get('quanhuyen')!='Chọn')?$request->get('quanhuyen'):'');
	    		$user->setPhuongxa(($request->get('phuongxa')!='Chọn')?$request->get('phuongxa'):'');
	    		$user->setTtkhac(($request->get('ttkhac')!=null)?$request->get('ttkhac'):'');
	    		$user->setHonnhan(($request->get('honnhan')!=null)?$request->get('honnhan'):'');
	    		$user->setChieucao(($request->get('chieucao')!=null)?$request->get('chieucao'):'');
	    		$user->setNhommau(($request->get('nhommau')!=null)?$request->get('nhommau'):'');
	    		$user->setXuatthan(($request->get('xuatthan')!=null)?$request->get('xuatthan'):'');
	    		$user->setSothich(($request->get('sothich')!=null)?$request->get('sothich'):'');
         
                $user->setDaotao(json_encode($request->get('daotao')!=null?$request->get('daotao'):'',JSON_UNESCAPED_UNICODE));
                $user->setNganh(json_encode($request->get('nganh')!=null?$request->get('nganh'):'',JSON_UNESCAPED_UNICODE));

            
                 
                $user->setTrinhdo(json_encode($request->get('trinhdo'),JSON_UNESCAPED_UNICODE));
                $user->setNgoaingu(json_encode($request->get('ngoaingu'),JSON_UNESCAPED_UNICODE));
               
	    		$user->setMoiquanhe(json_encode($request->get('moiquanhe'),JSON_UNESCAPED_UNICODE));
	    		$user->setHoten(json_encode($request->get('hoten'),JSON_UNESCAPED_UNICODE));
	    		$user->setDiachi(json_encode($request->get('diachi'),JSON_UNESCAPED_UNICODE));
	    		$user->setState(true);
                $user->setHovaten($request->get('ho')." ".$request->get('ten'));

    	
    		
    		if ($request->get('ten') == null) {
    			$errors['ten'] = "Bạn không được bỏ trống Tên !";
    		}
            if (strlen($request->get('ten')) > 30 ) {
                $errors['ten'] = "Bạn không được nhập tên có độ dài quá 30 ký tự !";
            }
            if (strlen($request->get('ho')) > 30 ) {
                $errors['ho'] = "Bạn không được nhập họ có độ dài quá 30 ký tự !";
            }
    		if ($request->get('ho') == null) {
    			$errors['ho'] = "Bạn không được bỏ trống Họ !";
    		}
    		if ($request->get('ngaysinh') == null) {
    			$errors['ngaysinh'] = "Bạn không được bỏ trống ngày sinh !";
    		}
    		if ($request->get('email') == null) {
    			$errors['email'] = "Bạn không được bỏ trống email !";
    		}
    		if ($request->files->get('anh') == null) {
    			$errors['anh'] = "Bạn không được bỏ trống ảnh !";
    		}
    		if ($request->get('didong') == null) {
    			$errors['didong'] = "Bạn không được bỏ trống số điện thoại di động !";
    		}



    		if (!$errors) {
                
	    		$user->save();
                 move_uploaded_file($files->getPathName(), 'userfiles/'.$files->getClientOriginalName());
                $notify="Bạn đã thêm nhân viên thành công !";
                $session->getFlashBag()->add('notify', $notify);
	    		return $this->redirectToRoute('index');
    		}
    		else{
		    	$mqh=json_decode($user->getMoiquanhe());
				$ht=json_decode($user->getHoten());
				$dc=json_decode($user->getDiachi());
                $daotao=json_decode($user->getDaoTao());
                $nganh=json_decode($user->getNganh());
                $trinhdo=json_decode($user->getTrinhdo());
                $ngoaingu=json_decode($user->getNgoaingu());

   				$this->view_data['manv'] = $manv;
                $this->view_data['mqh'] = $mqh;
                $this->view_data['ht']  = $ht;
                $this->view_data['user'] = $user;
                $this->view_data['diachi'] = $dc;
                $this->view_data['daotao'] = $daotao;
                $this->view_data['nganh'] = $nganh;
                $this->view_data['ngoaingu'] = $ngoaingu;
                $this->view_data['trinhdo'] = $trinhdo;
                $this->view_data['errors'] = $errors;
                $this->view_data['acc']=$session->get('account');
                $this->view_data['img']=$session->get('img');

    			return $this->render('@App/add.html.twig',$this->view_data);
    		}	
    	}    
         if($session->get('account')==null){
          return $this->render('@App/warning.html.twig');
        }
    	return $this->render('@App/add.html.twig',['acc'=>$session->get('account'),'img'=>$session->get('img'),'manv'=>$manv]);
    }	
    /**
     * @Route("/user/edit/{id}", name="user_edit")
     */
    public function editUser(Request $request , $id){
    	$errors=array();
        $session=new Session;
    	if ($request->isMethod('POST')) {
            $files = $request->files->get('anh');
    		$user_edit = NhansuQuery::create()->filterById($id)->findOne();
    		$user_edit->setHo(($request->get('ho')!=null)?$request->get('ho'):'');
    		$user_edit->setTen(($request->get('ten')!=null)?$request->get('ten'):'');
    		$user_edit->setGioitinh(($request->get('gioitinh')!=null)?$request->get('gioitinh'):'');
    		$user_edit->setNgaysinh(($request->get('ngaysinh')!=null)?$request->get('ngaysinh'):$user_edit->getNgaysinh());;
    		$user_edit->setQuoctich(($request->get('quoctich')!=null)?$request->get('quoctich'):'');
    		$user_edit->setDantoc(($request->get('dantoc')!=null)?$request->get('dantoc'):'');
    		$user_edit->setTongiao(($request->get('tongiao')!=null)?$request->get('tongiao'):'');
    		$user_edit->setSocmnd(($request->get('soCMND')!=null)?$request->get('soCMND'):'');
    		$user_edit->setNgaycap(($request->get('ngaycap')!=null)?$request->get('ngaycap'):'');
    		$user_edit->setNoicap(($request->get('noicap')!=null)?$request->get('noicap'):'');
            $user_edit->setAnh($files != null ? $files->getClientOriginalName() : $user_edit->getAnh());

    		$user_edit->setDidong(($request->get('didong')!=null)?$request->get('didong'):'');
    		$user_edit->setEmail(($request->get('email')!=null)?$request->get('email'):'');
    		$user_edit->setSkype(($request->get('skype')!=null)?$request->get('skype'):'');
    		$user_edit->setFacebook(($request->get('facebook')!=null)?$request->get('facebook'):'');
    		$user_edit->setQuocgia(($request->get('quocgia')!='Chọn')?$request->get('quocgia'):'');
    		$user_edit->setTinhthanh(($request->get('tinhthanh')!='Chọn')?$request->get('tinhthanh'):'');
    		$user_edit->setQuanhuyen(($request->get('quanhuyen')!='Chọn')?$request->get('quanhuyen'):'');
    		$user_edit->setPhuongxa(($request->get('phuongxa')!='Chọn')?$request->get('phuongxa'):'');
    		$user_edit->setTtkhac(($request->get('ttkhac')!=null)?$request->get('ttkhac'):'');
    		$user_edit->setHonnhan(($request->get('honnhan')!=null)?$request->get('honnhan'):'');
    		$user_edit->setChieucao(($request->get('chieucao')!=null)?$request->get('chieucao'):'');
    		$user_edit->setNhommau(($request->get('nhommau')!=null)?$request->get('nhommau'):'');
    		$user_edit->setXuatthan(($request->get('xuatthan')!=null)?$request->get('xuatthan'):'');
    		$user_edit->setSothich(($request->get('sothich')!=null)?$request->get('sothich'):'');

            $user_edit->setTrinhdo(json_encode($request->get('trinhdo'),JSON_UNESCAPED_UNICODE));
            $user_edit->setNganh(json_encode($request->get('nganh'),JSON_UNESCAPED_UNICODE));
            $user_edit->setNgoaingu(json_encode($request->get('ngoaingu'),JSON_UNESCAPED_UNICODE));
            $user_edit->setDaotao(json_encode($request->get('daotao'),JSON_UNESCAPED_UNICODE));
    	
    		$user_edit->setMoiquanhe(json_encode($request->get('moiquanhe'),JSON_UNESCAPED_UNICODE));
    		$user_edit->setHoten(json_encode($request->get('hoten'),JSON_UNESCAPED_UNICODE));
    		$user_edit->setDiachi(json_encode($request->get('diachi'),JSON_UNESCAPED_UNICODE));
            $user_edit->setHovaten($request->get('ho')." ".$request->get('ten'));
    	
 			
 			
    		if ($request->get('ten') == null) {
    			$errors['ten'] = "Bạn không được bỏ trống Tên !";
    		}
    		if ($request->get('ho') == null) {
    			$errors['ho'] = "Bạn không được bỏ trống Họ !";
    		}
    		if ($request->get('email') == null) {
    			$errors['email'] = "Bạn không được bỏ trống email !";
    		}
    		if ($request->get('didong') == null) {
    			$errors['didong'] = "Bạn không được bỏ trống số điện thoại di động !";
    		}
    		if (!$errors) {
                
	    		$user_edit->save();
                if($files!=null){
                    move_uploaded_file($files->getPathName(), 'userfiles/'.$files->getClientOriginalName()); 
                }
                $session->getFlashBag()->add('notify', 'Bạn đã sửa thông tin thành công !');
             
               
                return $this->redirectToRoute('index');
          
    		}
    	}
	    	$user = NhansuQuery::create()->filterById($id)->findOne();
            $view=array();
	    	$view['mqh']=$mqh=json_decode($user->getMoiquanhe());
			$view['ht']=$ht=json_decode($user->getHoten());
			$dc=json_decode($user->getDiachi());
            // dump($view);die;
            $daotao=json_decode($user->getDaoTao());
            $nganh=json_decode($user->getNganh());
            $trinhdo=json_decode($user->getTrinhdo());
            $ngoaingu=json_decode($user->getNgoaingu());

            $this->view_data['mqh'] = $mqh;
            $this->view_data['hoten']  = $ht;
            $this->view_data['user'] = $user;
            $this->view_data['diachi'] = $dc;
            $this->view_data['daotao'] = $daotao;
            $this->view_data['nganh'] = $nganh;
            $this->view_data['ngoaingu'] = $ngoaingu;
            $this->view_data['trinhdo'] = $trinhdo;
            $this->view_data['errors'] = $errors;
            $this->view_data['acc']= $session->get('account');
            $this->view_data['img']= $session->get('img');
         if($session->get('account')==null){
          return $this->render('@App/warning.html.twig');
        }
    		return $this->render('@App/edit.html.twig', $this->view_data);
    }
    /**
     * @Route("/user/delete/{id}", name="user_del")
     */
    public function deleteUser($id){

    	$user = NhansuQuery::create()->filterById($id)->findOne();
        if($user!=null){
            $user->setState(false);
            $user->save(); 
            echo json_encode(array(
                'Mess' => 'Bạn đã xóa thành công !',
                'Code' => 1
            ),JSON_UNESCAPED_UNICODE);
            exit();

        }
        echo json_encode(array(
                'Mess' => 'Bạn đã xóa thất bại !',
                'Code' => 2
            ),JSON_UNESCAPED_UNICODE);
         exit();
    	//$user->delete();
        // $session = new Session();
        // $notify="Bạn đã xóa thành công !";
        // $session->set('notify',$notify);
        //return $this->redirectToRoute('index');
    }
    /**
     * @Route("/user/info/{id}", name="user_info")
     */
    public function infoUserAction(Request $request,$id)
    {
        $session=new Session;
		$user_info = NhansuQuery::create()->filterById($id)->findOne();
		$mqh=json_decode($user_info->getMoiquanhe());
		$ht=json_decode($user_info->getHoten());
		$dc=json_decode($user_info->getDiachi());
        $daotao=json_decode($user_info->getDaoTao());
        $nganh=json_decode($user_info->getNganh());
        $trinhdo=json_decode($user_info->getTrinhdo());
        $ngoaingu=json_decode($user_info->getNgoaingu());
        $this->view_data['mqh'] = $mqh;
        $this->view_data['ht']  = $ht;
        $this->view_data['user'] = $user_info ;
        $this->view_data['diachi'] = $dc;
        $this->view_data['daotao'] = $daotao;
        $this->view_data['nganh'] = $nganh;
        $this->view_data['ngoaingu'] = $ngoaingu;
        $this->view_data['trinhdo'] = $trinhdo;
        $this->view_data['acc']=$session->get('account');
        $this->view_data['img']=$session->get('img');
		// dump($ht);die;
         if($session->get('account')==null){
          return $this->render('@App/warning.html.twig');
        }
		return $this->render('@App/info.html.twig',$this->view_data);
    	
    }
    
}

