<?php

namespace AppBundle\Model\Map;

use AppBundle\Model\Nhansu;
use AppBundle\Model\NhansuQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'nhansu' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class NhansuTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src\AppBundle.Model.Map.NhansuTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'nhansu';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\AppBundle\\Model\\Nhansu';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'src\AppBundle.Model.Nhansu';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 36;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 36;

    /**
     * the column name for the id field
     */
    const COL_ID = 'nhansu.id';

    /**
     * the column name for the ma_nv field
     */
    const COL_MA_NV = 'nhansu.ma_nv';

    /**
     * the column name for the ho field
     */
    const COL_HO = 'nhansu.ho';

    /**
     * the column name for the ten field
     */
    const COL_TEN = 'nhansu.ten';

    /**
     * the column name for the gioitinh field
     */
    const COL_GIOITINH = 'nhansu.gioitinh';

    /**
     * the column name for the ngaysinh field
     */
    const COL_NGAYSINH = 'nhansu.ngaysinh';

    /**
     * the column name for the quoctich field
     */
    const COL_QUOCTICH = 'nhansu.quoctich';

    /**
     * the column name for the dantoc field
     */
    const COL_DANTOC = 'nhansu.dantoc';

    /**
     * the column name for the tongiao field
     */
    const COL_TONGIAO = 'nhansu.tongiao';

    /**
     * the column name for the so_cmnd field
     */
    const COL_SO_CMND = 'nhansu.so_cmnd';

    /**
     * the column name for the ngaycap field
     */
    const COL_NGAYCAP = 'nhansu.ngaycap';

    /**
     * the column name for the noicap field
     */
    const COL_NOICAP = 'nhansu.noicap';

    /**
     * the column name for the anh field
     */
    const COL_ANH = 'nhansu.anh';

    /**
     * the column name for the didong field
     */
    const COL_DIDONG = 'nhansu.didong';

    /**
     * the column name for the email field
     */
    const COL_EMAIL = 'nhansu.email';

    /**
     * the column name for the skype field
     */
    const COL_SKYPE = 'nhansu.skype';

    /**
     * the column name for the facebook field
     */
    const COL_FACEBOOK = 'nhansu.facebook';

    /**
     * the column name for the quocgia field
     */
    const COL_QUOCGIA = 'nhansu.quocgia';

    /**
     * the column name for the tinhthanh field
     */
    const COL_TINHTHANH = 'nhansu.tinhthanh';

    /**
     * the column name for the quanhuyen field
     */
    const COL_QUANHUYEN = 'nhansu.quanhuyen';

    /**
     * the column name for the phuongxa field
     */
    const COL_PHUONGXA = 'nhansu.phuongxa';

    /**
     * the column name for the ttkhac field
     */
    const COL_TTKHAC = 'nhansu.ttkhac';

    /**
     * the column name for the honnhan field
     */
    const COL_HONNHAN = 'nhansu.honnhan';

    /**
     * the column name for the chieucao field
     */
    const COL_CHIEUCAO = 'nhansu.chieucao';

    /**
     * the column name for the nhommau field
     */
    const COL_NHOMMAU = 'nhansu.nhommau';

    /**
     * the column name for the xuatthan field
     */
    const COL_XUATTHAN = 'nhansu.xuatthan';

    /**
     * the column name for the sothich field
     */
    const COL_SOTHICH = 'nhansu.sothich';

    /**
     * the column name for the trinhdo field
     */
    const COL_TRINHDO = 'nhansu.trinhdo';

    /**
     * the column name for the ngoaingu field
     */
    const COL_NGOAINGU = 'nhansu.ngoaingu';

    /**
     * the column name for the daotao field
     */
    const COL_DAOTAO = 'nhansu.daotao';

    /**
     * the column name for the nganh field
     */
    const COL_NGANH = 'nhansu.nganh';

    /**
     * the column name for the moiquanhe field
     */
    const COL_MOIQUANHE = 'nhansu.moiquanhe';

    /**
     * the column name for the hoten field
     */
    const COL_HOTEN = 'nhansu.hoten';

    /**
     * the column name for the diachi field
     */
    const COL_DIACHI = 'nhansu.diachi';

    /**
     * the column name for the hovaten field
     */
    const COL_HOVATEN = 'nhansu.hovaten';

    /**
     * the column name for the state field
     */
    const COL_STATE = 'nhansu.state';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'MaNv', 'Ho', 'Ten', 'Gioitinh', 'Ngaysinh', 'Quoctich', 'Dantoc', 'Tongiao', 'SoCmnd', 'Ngaycap', 'Noicap', 'Anh', 'Didong', 'Email', 'Skype', 'Facebook', 'Quocgia', 'Tinhthanh', 'Quanhuyen', 'Phuongxa', 'Ttkhac', 'Honnhan', 'Chieucao', 'Nhommau', 'Xuatthan', 'Sothich', 'Trinhdo', 'Ngoaingu', 'Daotao', 'Nganh', 'Moiquanhe', 'Hoten', 'Diachi', 'Hovaten', 'State', ),
        self::TYPE_CAMELNAME     => array('id', 'maNv', 'ho', 'ten', 'gioitinh', 'ngaysinh', 'quoctich', 'dantoc', 'tongiao', 'soCmnd', 'ngaycap', 'noicap', 'anh', 'didong', 'email', 'skype', 'facebook', 'quocgia', 'tinhthanh', 'quanhuyen', 'phuongxa', 'ttkhac', 'honnhan', 'chieucao', 'nhommau', 'xuatthan', 'sothich', 'trinhdo', 'ngoaingu', 'daotao', 'nganh', 'moiquanhe', 'hoten', 'diachi', 'hovaten', 'state', ),
        self::TYPE_COLNAME       => array(NhansuTableMap::COL_ID, NhansuTableMap::COL_MA_NV, NhansuTableMap::COL_HO, NhansuTableMap::COL_TEN, NhansuTableMap::COL_GIOITINH, NhansuTableMap::COL_NGAYSINH, NhansuTableMap::COL_QUOCTICH, NhansuTableMap::COL_DANTOC, NhansuTableMap::COL_TONGIAO, NhansuTableMap::COL_SO_CMND, NhansuTableMap::COL_NGAYCAP, NhansuTableMap::COL_NOICAP, NhansuTableMap::COL_ANH, NhansuTableMap::COL_DIDONG, NhansuTableMap::COL_EMAIL, NhansuTableMap::COL_SKYPE, NhansuTableMap::COL_FACEBOOK, NhansuTableMap::COL_QUOCGIA, NhansuTableMap::COL_TINHTHANH, NhansuTableMap::COL_QUANHUYEN, NhansuTableMap::COL_PHUONGXA, NhansuTableMap::COL_TTKHAC, NhansuTableMap::COL_HONNHAN, NhansuTableMap::COL_CHIEUCAO, NhansuTableMap::COL_NHOMMAU, NhansuTableMap::COL_XUATTHAN, NhansuTableMap::COL_SOTHICH, NhansuTableMap::COL_TRINHDO, NhansuTableMap::COL_NGOAINGU, NhansuTableMap::COL_DAOTAO, NhansuTableMap::COL_NGANH, NhansuTableMap::COL_MOIQUANHE, NhansuTableMap::COL_HOTEN, NhansuTableMap::COL_DIACHI, NhansuTableMap::COL_HOVATEN, NhansuTableMap::COL_STATE, ),
        self::TYPE_FIELDNAME     => array('id', 'ma_nv', 'ho', 'ten', 'gioitinh', 'ngaysinh', 'quoctich', 'dantoc', 'tongiao', 'so_cmnd', 'ngaycap', 'noicap', 'anh', 'didong', 'email', 'skype', 'facebook', 'quocgia', 'tinhthanh', 'quanhuyen', 'phuongxa', 'ttkhac', 'honnhan', 'chieucao', 'nhommau', 'xuatthan', 'sothich', 'trinhdo', 'ngoaingu', 'daotao', 'nganh', 'moiquanhe', 'hoten', 'diachi', 'hovaten', 'state', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'MaNv' => 1, 'Ho' => 2, 'Ten' => 3, 'Gioitinh' => 4, 'Ngaysinh' => 5, 'Quoctich' => 6, 'Dantoc' => 7, 'Tongiao' => 8, 'SoCmnd' => 9, 'Ngaycap' => 10, 'Noicap' => 11, 'Anh' => 12, 'Didong' => 13, 'Email' => 14, 'Skype' => 15, 'Facebook' => 16, 'Quocgia' => 17, 'Tinhthanh' => 18, 'Quanhuyen' => 19, 'Phuongxa' => 20, 'Ttkhac' => 21, 'Honnhan' => 22, 'Chieucao' => 23, 'Nhommau' => 24, 'Xuatthan' => 25, 'Sothich' => 26, 'Trinhdo' => 27, 'Ngoaingu' => 28, 'Daotao' => 29, 'Nganh' => 30, 'Moiquanhe' => 31, 'Hoten' => 32, 'Diachi' => 33, 'Hovaten' => 34, 'State' => 35, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'maNv' => 1, 'ho' => 2, 'ten' => 3, 'gioitinh' => 4, 'ngaysinh' => 5, 'quoctich' => 6, 'dantoc' => 7, 'tongiao' => 8, 'soCmnd' => 9, 'ngaycap' => 10, 'noicap' => 11, 'anh' => 12, 'didong' => 13, 'email' => 14, 'skype' => 15, 'facebook' => 16, 'quocgia' => 17, 'tinhthanh' => 18, 'quanhuyen' => 19, 'phuongxa' => 20, 'ttkhac' => 21, 'honnhan' => 22, 'chieucao' => 23, 'nhommau' => 24, 'xuatthan' => 25, 'sothich' => 26, 'trinhdo' => 27, 'ngoaingu' => 28, 'daotao' => 29, 'nganh' => 30, 'moiquanhe' => 31, 'hoten' => 32, 'diachi' => 33, 'hovaten' => 34, 'state' => 35, ),
        self::TYPE_COLNAME       => array(NhansuTableMap::COL_ID => 0, NhansuTableMap::COL_MA_NV => 1, NhansuTableMap::COL_HO => 2, NhansuTableMap::COL_TEN => 3, NhansuTableMap::COL_GIOITINH => 4, NhansuTableMap::COL_NGAYSINH => 5, NhansuTableMap::COL_QUOCTICH => 6, NhansuTableMap::COL_DANTOC => 7, NhansuTableMap::COL_TONGIAO => 8, NhansuTableMap::COL_SO_CMND => 9, NhansuTableMap::COL_NGAYCAP => 10, NhansuTableMap::COL_NOICAP => 11, NhansuTableMap::COL_ANH => 12, NhansuTableMap::COL_DIDONG => 13, NhansuTableMap::COL_EMAIL => 14, NhansuTableMap::COL_SKYPE => 15, NhansuTableMap::COL_FACEBOOK => 16, NhansuTableMap::COL_QUOCGIA => 17, NhansuTableMap::COL_TINHTHANH => 18, NhansuTableMap::COL_QUANHUYEN => 19, NhansuTableMap::COL_PHUONGXA => 20, NhansuTableMap::COL_TTKHAC => 21, NhansuTableMap::COL_HONNHAN => 22, NhansuTableMap::COL_CHIEUCAO => 23, NhansuTableMap::COL_NHOMMAU => 24, NhansuTableMap::COL_XUATTHAN => 25, NhansuTableMap::COL_SOTHICH => 26, NhansuTableMap::COL_TRINHDO => 27, NhansuTableMap::COL_NGOAINGU => 28, NhansuTableMap::COL_DAOTAO => 29, NhansuTableMap::COL_NGANH => 30, NhansuTableMap::COL_MOIQUANHE => 31, NhansuTableMap::COL_HOTEN => 32, NhansuTableMap::COL_DIACHI => 33, NhansuTableMap::COL_HOVATEN => 34, NhansuTableMap::COL_STATE => 35, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'ma_nv' => 1, 'ho' => 2, 'ten' => 3, 'gioitinh' => 4, 'ngaysinh' => 5, 'quoctich' => 6, 'dantoc' => 7, 'tongiao' => 8, 'so_cmnd' => 9, 'ngaycap' => 10, 'noicap' => 11, 'anh' => 12, 'didong' => 13, 'email' => 14, 'skype' => 15, 'facebook' => 16, 'quocgia' => 17, 'tinhthanh' => 18, 'quanhuyen' => 19, 'phuongxa' => 20, 'ttkhac' => 21, 'honnhan' => 22, 'chieucao' => 23, 'nhommau' => 24, 'xuatthan' => 25, 'sothich' => 26, 'trinhdo' => 27, 'ngoaingu' => 28, 'daotao' => 29, 'nganh' => 30, 'moiquanhe' => 31, 'hoten' => 32, 'diachi' => 33, 'hovaten' => 34, 'state' => 35, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('nhansu');
        $this->setPhpName('Nhansu');
        $this->setIdentifierQuoting(true);
        $this->setClassName('\\AppBundle\\Model\\Nhansu');
        $this->setPackage('src\AppBundle.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('ma_nv', 'MaNv', 'VARCHAR', false, 11, null);
        $this->addColumn('ho', 'Ho', 'VARCHAR', false, 30, null);
        $this->addColumn('ten', 'Ten', 'VARCHAR', false, 30, null);
        $this->addColumn('gioitinh', 'Gioitinh', 'VARCHAR', false, 30, null);
        $this->addColumn('ngaysinh', 'Ngaysinh', 'DATE', false, null, null);
        $this->addColumn('quoctich', 'Quoctich', 'VARCHAR', false, 100, null);
        $this->addColumn('dantoc', 'Dantoc', 'VARCHAR', false, 50, null);
        $this->addColumn('tongiao', 'Tongiao', 'VARCHAR', false, 50, null);
        $this->addColumn('so_cmnd', 'SoCmnd', 'VARCHAR', false, 50, null);
        $this->addColumn('ngaycap', 'Ngaycap', 'DATE', false, null, null);
        $this->addColumn('noicap', 'Noicap', 'VARCHAR', false, 50, null);
        $this->addColumn('anh', 'Anh', 'VARCHAR', false, 50, null);
        $this->addColumn('didong', 'Didong', 'VARCHAR', false, 50, null);
        $this->addColumn('email', 'Email', 'VARCHAR', false, 50, null);
        $this->addColumn('skype', 'Skype', 'VARCHAR', false, 50, null);
        $this->addColumn('facebook', 'Facebook', 'VARCHAR', false, 50, null);
        $this->addColumn('quocgia', 'Quocgia', 'VARCHAR', false, 50, null);
        $this->addColumn('tinhthanh', 'Tinhthanh', 'VARCHAR', false, 50, null);
        $this->addColumn('quanhuyen', 'Quanhuyen', 'VARCHAR', false, 50, null);
        $this->addColumn('phuongxa', 'Phuongxa', 'VARCHAR', false, 50, null);
        $this->addColumn('ttkhac', 'Ttkhac', 'VARCHAR', false, 200, null);
        $this->addColumn('honnhan', 'Honnhan', 'VARCHAR', false, 50, null);
        $this->addColumn('chieucao', 'Chieucao', 'VARCHAR', false, 50, null);
        $this->addColumn('nhommau', 'Nhommau', 'VARCHAR', false, 50, null);
        $this->addColumn('xuatthan', 'Xuatthan', 'VARCHAR', false, 100, null);
        $this->addColumn('sothich', 'Sothich', 'VARCHAR', false, 500, null);
        $this->addColumn('trinhdo', 'Trinhdo', 'VARCHAR', false, 100, null);
        $this->addColumn('ngoaingu', 'Ngoaingu', 'VARCHAR', false, 100, null);
        $this->addColumn('daotao', 'Daotao', 'VARCHAR', false, 100, null);
        $this->addColumn('nganh', 'Nganh', 'VARCHAR', false, 50, null);
        $this->addColumn('moiquanhe', 'Moiquanhe', 'VARCHAR', false, 50, null);
        $this->addColumn('hoten', 'Hoten', 'VARCHAR', false, 30, null);
        $this->addColumn('diachi', 'Diachi', 'VARCHAR', false, 200, null);
        $this->addColumn('hovaten', 'Hovaten', 'VARCHAR', false, 100, null);
        $this->addColumn('state', 'State', 'TINYINT', false, 1, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? NhansuTableMap::CLASS_DEFAULT : NhansuTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (Nhansu object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = NhansuTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = NhansuTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + NhansuTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = NhansuTableMap::OM_CLASS;
            /** @var Nhansu $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            NhansuTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = NhansuTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = NhansuTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var Nhansu $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                NhansuTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(NhansuTableMap::COL_ID);
            $criteria->addSelectColumn(NhansuTableMap::COL_MA_NV);
            $criteria->addSelectColumn(NhansuTableMap::COL_HO);
            $criteria->addSelectColumn(NhansuTableMap::COL_TEN);
            $criteria->addSelectColumn(NhansuTableMap::COL_GIOITINH);
            $criteria->addSelectColumn(NhansuTableMap::COL_NGAYSINH);
            $criteria->addSelectColumn(NhansuTableMap::COL_QUOCTICH);
            $criteria->addSelectColumn(NhansuTableMap::COL_DANTOC);
            $criteria->addSelectColumn(NhansuTableMap::COL_TONGIAO);
            $criteria->addSelectColumn(NhansuTableMap::COL_SO_CMND);
            $criteria->addSelectColumn(NhansuTableMap::COL_NGAYCAP);
            $criteria->addSelectColumn(NhansuTableMap::COL_NOICAP);
            $criteria->addSelectColumn(NhansuTableMap::COL_ANH);
            $criteria->addSelectColumn(NhansuTableMap::COL_DIDONG);
            $criteria->addSelectColumn(NhansuTableMap::COL_EMAIL);
            $criteria->addSelectColumn(NhansuTableMap::COL_SKYPE);
            $criteria->addSelectColumn(NhansuTableMap::COL_FACEBOOK);
            $criteria->addSelectColumn(NhansuTableMap::COL_QUOCGIA);
            $criteria->addSelectColumn(NhansuTableMap::COL_TINHTHANH);
            $criteria->addSelectColumn(NhansuTableMap::COL_QUANHUYEN);
            $criteria->addSelectColumn(NhansuTableMap::COL_PHUONGXA);
            $criteria->addSelectColumn(NhansuTableMap::COL_TTKHAC);
            $criteria->addSelectColumn(NhansuTableMap::COL_HONNHAN);
            $criteria->addSelectColumn(NhansuTableMap::COL_CHIEUCAO);
            $criteria->addSelectColumn(NhansuTableMap::COL_NHOMMAU);
            $criteria->addSelectColumn(NhansuTableMap::COL_XUATTHAN);
            $criteria->addSelectColumn(NhansuTableMap::COL_SOTHICH);
            $criteria->addSelectColumn(NhansuTableMap::COL_TRINHDO);
            $criteria->addSelectColumn(NhansuTableMap::COL_NGOAINGU);
            $criteria->addSelectColumn(NhansuTableMap::COL_DAOTAO);
            $criteria->addSelectColumn(NhansuTableMap::COL_NGANH);
            $criteria->addSelectColumn(NhansuTableMap::COL_MOIQUANHE);
            $criteria->addSelectColumn(NhansuTableMap::COL_HOTEN);
            $criteria->addSelectColumn(NhansuTableMap::COL_DIACHI);
            $criteria->addSelectColumn(NhansuTableMap::COL_HOVATEN);
            $criteria->addSelectColumn(NhansuTableMap::COL_STATE);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.ma_nv');
            $criteria->addSelectColumn($alias . '.ho');
            $criteria->addSelectColumn($alias . '.ten');
            $criteria->addSelectColumn($alias . '.gioitinh');
            $criteria->addSelectColumn($alias . '.ngaysinh');
            $criteria->addSelectColumn($alias . '.quoctich');
            $criteria->addSelectColumn($alias . '.dantoc');
            $criteria->addSelectColumn($alias . '.tongiao');
            $criteria->addSelectColumn($alias . '.so_cmnd');
            $criteria->addSelectColumn($alias . '.ngaycap');
            $criteria->addSelectColumn($alias . '.noicap');
            $criteria->addSelectColumn($alias . '.anh');
            $criteria->addSelectColumn($alias . '.didong');
            $criteria->addSelectColumn($alias . '.email');
            $criteria->addSelectColumn($alias . '.skype');
            $criteria->addSelectColumn($alias . '.facebook');
            $criteria->addSelectColumn($alias . '.quocgia');
            $criteria->addSelectColumn($alias . '.tinhthanh');
            $criteria->addSelectColumn($alias . '.quanhuyen');
            $criteria->addSelectColumn($alias . '.phuongxa');
            $criteria->addSelectColumn($alias . '.ttkhac');
            $criteria->addSelectColumn($alias . '.honnhan');
            $criteria->addSelectColumn($alias . '.chieucao');
            $criteria->addSelectColumn($alias . '.nhommau');
            $criteria->addSelectColumn($alias . '.xuatthan');
            $criteria->addSelectColumn($alias . '.sothich');
            $criteria->addSelectColumn($alias . '.trinhdo');
            $criteria->addSelectColumn($alias . '.ngoaingu');
            $criteria->addSelectColumn($alias . '.daotao');
            $criteria->addSelectColumn($alias . '.nganh');
            $criteria->addSelectColumn($alias . '.moiquanhe');
            $criteria->addSelectColumn($alias . '.hoten');
            $criteria->addSelectColumn($alias . '.diachi');
            $criteria->addSelectColumn($alias . '.hovaten');
            $criteria->addSelectColumn($alias . '.state');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(NhansuTableMap::DATABASE_NAME)->getTable(NhansuTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(NhansuTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(NhansuTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new NhansuTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a Nhansu or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or Nhansu object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(NhansuTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \AppBundle\Model\Nhansu) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(NhansuTableMap::DATABASE_NAME);
            $criteria->add(NhansuTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = NhansuQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            NhansuTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                NhansuTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the nhansu table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return NhansuQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a Nhansu or Criteria object.
     *
     * @param mixed               $criteria Criteria or Nhansu object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(NhansuTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from Nhansu object
        }

        if ($criteria->containsKey(NhansuTableMap::COL_ID) && $criteria->keyContainsValue(NhansuTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.NhansuTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = NhansuQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // NhansuTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
NhansuTableMap::buildTableMap();
