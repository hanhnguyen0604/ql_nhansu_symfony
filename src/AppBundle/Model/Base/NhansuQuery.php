<?php

namespace AppBundle\Model\Base;

use \Exception;
use \PDO;
use AppBundle\Model\Nhansu as ChildNhansu;
use AppBundle\Model\NhansuQuery as ChildNhansuQuery;
use AppBundle\Model\Map\NhansuTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'nhansu' table.
 *
 *
 *
 * @method     ChildNhansuQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildNhansuQuery orderByMaNv($order = Criteria::ASC) Order by the ma_nv column
 * @method     ChildNhansuQuery orderByHo($order = Criteria::ASC) Order by the ho column
 * @method     ChildNhansuQuery orderByTen($order = Criteria::ASC) Order by the ten column
 * @method     ChildNhansuQuery orderByGioitinh($order = Criteria::ASC) Order by the gioitinh column
 * @method     ChildNhansuQuery orderByNgaysinh($order = Criteria::ASC) Order by the ngaysinh column
 * @method     ChildNhansuQuery orderByQuoctich($order = Criteria::ASC) Order by the quoctich column
 * @method     ChildNhansuQuery orderByDantoc($order = Criteria::ASC) Order by the dantoc column
 * @method     ChildNhansuQuery orderByTongiao($order = Criteria::ASC) Order by the tongiao column
 * @method     ChildNhansuQuery orderBySoCmnd($order = Criteria::ASC) Order by the so_cmnd column
 * @method     ChildNhansuQuery orderByNgaycap($order = Criteria::ASC) Order by the ngaycap column
 * @method     ChildNhansuQuery orderByNoicap($order = Criteria::ASC) Order by the noicap column
 * @method     ChildNhansuQuery orderByAnh($order = Criteria::ASC) Order by the anh column
 * @method     ChildNhansuQuery orderByDidong($order = Criteria::ASC) Order by the didong column
 * @method     ChildNhansuQuery orderByEmail($order = Criteria::ASC) Order by the email column
 * @method     ChildNhansuQuery orderBySkype($order = Criteria::ASC) Order by the skype column
 * @method     ChildNhansuQuery orderByFacebook($order = Criteria::ASC) Order by the facebook column
 * @method     ChildNhansuQuery orderByQuocgia($order = Criteria::ASC) Order by the quocgia column
 * @method     ChildNhansuQuery orderByTinhthanh($order = Criteria::ASC) Order by the tinhthanh column
 * @method     ChildNhansuQuery orderByQuanhuyen($order = Criteria::ASC) Order by the quanhuyen column
 * @method     ChildNhansuQuery orderByPhuongxa($order = Criteria::ASC) Order by the phuongxa column
 * @method     ChildNhansuQuery orderByTtkhac($order = Criteria::ASC) Order by the ttkhac column
 * @method     ChildNhansuQuery orderByHonnhan($order = Criteria::ASC) Order by the honnhan column
 * @method     ChildNhansuQuery orderByChieucao($order = Criteria::ASC) Order by the chieucao column
 * @method     ChildNhansuQuery orderByNhommau($order = Criteria::ASC) Order by the nhommau column
 * @method     ChildNhansuQuery orderByXuatthan($order = Criteria::ASC) Order by the xuatthan column
 * @method     ChildNhansuQuery orderBySothich($order = Criteria::ASC) Order by the sothich column
 * @method     ChildNhansuQuery orderByTrinhdo($order = Criteria::ASC) Order by the trinhdo column
 * @method     ChildNhansuQuery orderByNgoaingu($order = Criteria::ASC) Order by the ngoaingu column
 * @method     ChildNhansuQuery orderByDaotao($order = Criteria::ASC) Order by the daotao column
 * @method     ChildNhansuQuery orderByNganh($order = Criteria::ASC) Order by the nganh column
 * @method     ChildNhansuQuery orderByMoiquanhe($order = Criteria::ASC) Order by the moiquanhe column
 * @method     ChildNhansuQuery orderByHoten($order = Criteria::ASC) Order by the hoten column
 * @method     ChildNhansuQuery orderByDiachi($order = Criteria::ASC) Order by the diachi column
 * @method     ChildNhansuQuery orderByHovaten($order = Criteria::ASC) Order by the hovaten column
 * @method     ChildNhansuQuery orderByState($order = Criteria::ASC) Order by the state column
 *
 * @method     ChildNhansuQuery groupById() Group by the id column
 * @method     ChildNhansuQuery groupByMaNv() Group by the ma_nv column
 * @method     ChildNhansuQuery groupByHo() Group by the ho column
 * @method     ChildNhansuQuery groupByTen() Group by the ten column
 * @method     ChildNhansuQuery groupByGioitinh() Group by the gioitinh column
 * @method     ChildNhansuQuery groupByNgaysinh() Group by the ngaysinh column
 * @method     ChildNhansuQuery groupByQuoctich() Group by the quoctich column
 * @method     ChildNhansuQuery groupByDantoc() Group by the dantoc column
 * @method     ChildNhansuQuery groupByTongiao() Group by the tongiao column
 * @method     ChildNhansuQuery groupBySoCmnd() Group by the so_cmnd column
 * @method     ChildNhansuQuery groupByNgaycap() Group by the ngaycap column
 * @method     ChildNhansuQuery groupByNoicap() Group by the noicap column
 * @method     ChildNhansuQuery groupByAnh() Group by the anh column
 * @method     ChildNhansuQuery groupByDidong() Group by the didong column
 * @method     ChildNhansuQuery groupByEmail() Group by the email column
 * @method     ChildNhansuQuery groupBySkype() Group by the skype column
 * @method     ChildNhansuQuery groupByFacebook() Group by the facebook column
 * @method     ChildNhansuQuery groupByQuocgia() Group by the quocgia column
 * @method     ChildNhansuQuery groupByTinhthanh() Group by the tinhthanh column
 * @method     ChildNhansuQuery groupByQuanhuyen() Group by the quanhuyen column
 * @method     ChildNhansuQuery groupByPhuongxa() Group by the phuongxa column
 * @method     ChildNhansuQuery groupByTtkhac() Group by the ttkhac column
 * @method     ChildNhansuQuery groupByHonnhan() Group by the honnhan column
 * @method     ChildNhansuQuery groupByChieucao() Group by the chieucao column
 * @method     ChildNhansuQuery groupByNhommau() Group by the nhommau column
 * @method     ChildNhansuQuery groupByXuatthan() Group by the xuatthan column
 * @method     ChildNhansuQuery groupBySothich() Group by the sothich column
 * @method     ChildNhansuQuery groupByTrinhdo() Group by the trinhdo column
 * @method     ChildNhansuQuery groupByNgoaingu() Group by the ngoaingu column
 * @method     ChildNhansuQuery groupByDaotao() Group by the daotao column
 * @method     ChildNhansuQuery groupByNganh() Group by the nganh column
 * @method     ChildNhansuQuery groupByMoiquanhe() Group by the moiquanhe column
 * @method     ChildNhansuQuery groupByHoten() Group by the hoten column
 * @method     ChildNhansuQuery groupByDiachi() Group by the diachi column
 * @method     ChildNhansuQuery groupByHovaten() Group by the hovaten column
 * @method     ChildNhansuQuery groupByState() Group by the state column
 *
 * @method     ChildNhansuQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildNhansuQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildNhansuQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildNhansuQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildNhansuQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildNhansuQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildNhansu findOne(ConnectionInterface $con = null) Return the first ChildNhansu matching the query
 * @method     ChildNhansu findOneOrCreate(ConnectionInterface $con = null) Return the first ChildNhansu matching the query, or a new ChildNhansu object populated from the query conditions when no match is found
 *
 * @method     ChildNhansu findOneById(int $id) Return the first ChildNhansu filtered by the id column
 * @method     ChildNhansu findOneByMaNv(string $ma_nv) Return the first ChildNhansu filtered by the ma_nv column
 * @method     ChildNhansu findOneByHo(string $ho) Return the first ChildNhansu filtered by the ho column
 * @method     ChildNhansu findOneByTen(string $ten) Return the first ChildNhansu filtered by the ten column
 * @method     ChildNhansu findOneByGioitinh(string $gioitinh) Return the first ChildNhansu filtered by the gioitinh column
 * @method     ChildNhansu findOneByNgaysinh(string $ngaysinh) Return the first ChildNhansu filtered by the ngaysinh column
 * @method     ChildNhansu findOneByQuoctich(string $quoctich) Return the first ChildNhansu filtered by the quoctich column
 * @method     ChildNhansu findOneByDantoc(string $dantoc) Return the first ChildNhansu filtered by the dantoc column
 * @method     ChildNhansu findOneByTongiao(string $tongiao) Return the first ChildNhansu filtered by the tongiao column
 * @method     ChildNhansu findOneBySoCmnd(string $so_cmnd) Return the first ChildNhansu filtered by the so_cmnd column
 * @method     ChildNhansu findOneByNgaycap(string $ngaycap) Return the first ChildNhansu filtered by the ngaycap column
 * @method     ChildNhansu findOneByNoicap(string $noicap) Return the first ChildNhansu filtered by the noicap column
 * @method     ChildNhansu findOneByAnh(string $anh) Return the first ChildNhansu filtered by the anh column
 * @method     ChildNhansu findOneByDidong(string $didong) Return the first ChildNhansu filtered by the didong column
 * @method     ChildNhansu findOneByEmail(string $email) Return the first ChildNhansu filtered by the email column
 * @method     ChildNhansu findOneBySkype(string $skype) Return the first ChildNhansu filtered by the skype column
 * @method     ChildNhansu findOneByFacebook(string $facebook) Return the first ChildNhansu filtered by the facebook column
 * @method     ChildNhansu findOneByQuocgia(string $quocgia) Return the first ChildNhansu filtered by the quocgia column
 * @method     ChildNhansu findOneByTinhthanh(string $tinhthanh) Return the first ChildNhansu filtered by the tinhthanh column
 * @method     ChildNhansu findOneByQuanhuyen(string $quanhuyen) Return the first ChildNhansu filtered by the quanhuyen column
 * @method     ChildNhansu findOneByPhuongxa(string $phuongxa) Return the first ChildNhansu filtered by the phuongxa column
 * @method     ChildNhansu findOneByTtkhac(string $ttkhac) Return the first ChildNhansu filtered by the ttkhac column
 * @method     ChildNhansu findOneByHonnhan(string $honnhan) Return the first ChildNhansu filtered by the honnhan column
 * @method     ChildNhansu findOneByChieucao(string $chieucao) Return the first ChildNhansu filtered by the chieucao column
 * @method     ChildNhansu findOneByNhommau(string $nhommau) Return the first ChildNhansu filtered by the nhommau column
 * @method     ChildNhansu findOneByXuatthan(string $xuatthan) Return the first ChildNhansu filtered by the xuatthan column
 * @method     ChildNhansu findOneBySothich(string $sothich) Return the first ChildNhansu filtered by the sothich column
 * @method     ChildNhansu findOneByTrinhdo(string $trinhdo) Return the first ChildNhansu filtered by the trinhdo column
 * @method     ChildNhansu findOneByNgoaingu(string $ngoaingu) Return the first ChildNhansu filtered by the ngoaingu column
 * @method     ChildNhansu findOneByDaotao(string $daotao) Return the first ChildNhansu filtered by the daotao column
 * @method     ChildNhansu findOneByNganh(string $nganh) Return the first ChildNhansu filtered by the nganh column
 * @method     ChildNhansu findOneByMoiquanhe(string $moiquanhe) Return the first ChildNhansu filtered by the moiquanhe column
 * @method     ChildNhansu findOneByHoten(string $hoten) Return the first ChildNhansu filtered by the hoten column
 * @method     ChildNhansu findOneByDiachi(string $diachi) Return the first ChildNhansu filtered by the diachi column
 * @method     ChildNhansu findOneByHovaten(string $hovaten) Return the first ChildNhansu filtered by the hovaten column
 * @method     ChildNhansu findOneByState(int $state) Return the first ChildNhansu filtered by the state column *

 * @method     ChildNhansu requirePk($key, ConnectionInterface $con = null) Return the ChildNhansu by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildNhansu requireOne(ConnectionInterface $con = null) Return the first ChildNhansu matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildNhansu requireOneById(int $id) Return the first ChildNhansu filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildNhansu requireOneByMaNv(string $ma_nv) Return the first ChildNhansu filtered by the ma_nv column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildNhansu requireOneByHo(string $ho) Return the first ChildNhansu filtered by the ho column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildNhansu requireOneByTen(string $ten) Return the first ChildNhansu filtered by the ten column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildNhansu requireOneByGioitinh(string $gioitinh) Return the first ChildNhansu filtered by the gioitinh column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildNhansu requireOneByNgaysinh(string $ngaysinh) Return the first ChildNhansu filtered by the ngaysinh column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildNhansu requireOneByQuoctich(string $quoctich) Return the first ChildNhansu filtered by the quoctich column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildNhansu requireOneByDantoc(string $dantoc) Return the first ChildNhansu filtered by the dantoc column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildNhansu requireOneByTongiao(string $tongiao) Return the first ChildNhansu filtered by the tongiao column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildNhansu requireOneBySoCmnd(string $so_cmnd) Return the first ChildNhansu filtered by the so_cmnd column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildNhansu requireOneByNgaycap(string $ngaycap) Return the first ChildNhansu filtered by the ngaycap column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildNhansu requireOneByNoicap(string $noicap) Return the first ChildNhansu filtered by the noicap column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildNhansu requireOneByAnh(string $anh) Return the first ChildNhansu filtered by the anh column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildNhansu requireOneByDidong(string $didong) Return the first ChildNhansu filtered by the didong column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildNhansu requireOneByEmail(string $email) Return the first ChildNhansu filtered by the email column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildNhansu requireOneBySkype(string $skype) Return the first ChildNhansu filtered by the skype column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildNhansu requireOneByFacebook(string $facebook) Return the first ChildNhansu filtered by the facebook column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildNhansu requireOneByQuocgia(string $quocgia) Return the first ChildNhansu filtered by the quocgia column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildNhansu requireOneByTinhthanh(string $tinhthanh) Return the first ChildNhansu filtered by the tinhthanh column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildNhansu requireOneByQuanhuyen(string $quanhuyen) Return the first ChildNhansu filtered by the quanhuyen column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildNhansu requireOneByPhuongxa(string $phuongxa) Return the first ChildNhansu filtered by the phuongxa column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildNhansu requireOneByTtkhac(string $ttkhac) Return the first ChildNhansu filtered by the ttkhac column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildNhansu requireOneByHonnhan(string $honnhan) Return the first ChildNhansu filtered by the honnhan column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildNhansu requireOneByChieucao(string $chieucao) Return the first ChildNhansu filtered by the chieucao column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildNhansu requireOneByNhommau(string $nhommau) Return the first ChildNhansu filtered by the nhommau column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildNhansu requireOneByXuatthan(string $xuatthan) Return the first ChildNhansu filtered by the xuatthan column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildNhansu requireOneBySothich(string $sothich) Return the first ChildNhansu filtered by the sothich column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildNhansu requireOneByTrinhdo(string $trinhdo) Return the first ChildNhansu filtered by the trinhdo column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildNhansu requireOneByNgoaingu(string $ngoaingu) Return the first ChildNhansu filtered by the ngoaingu column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildNhansu requireOneByDaotao(string $daotao) Return the first ChildNhansu filtered by the daotao column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildNhansu requireOneByNganh(string $nganh) Return the first ChildNhansu filtered by the nganh column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildNhansu requireOneByMoiquanhe(string $moiquanhe) Return the first ChildNhansu filtered by the moiquanhe column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildNhansu requireOneByHoten(string $hoten) Return the first ChildNhansu filtered by the hoten column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildNhansu requireOneByDiachi(string $diachi) Return the first ChildNhansu filtered by the diachi column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildNhansu requireOneByHovaten(string $hovaten) Return the first ChildNhansu filtered by the hovaten column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildNhansu requireOneByState(int $state) Return the first ChildNhansu filtered by the state column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildNhansu[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildNhansu objects based on current ModelCriteria
 * @method     ChildNhansu[]|ObjectCollection findById(int $id) Return ChildNhansu objects filtered by the id column
 * @method     ChildNhansu[]|ObjectCollection findByMaNv(string $ma_nv) Return ChildNhansu objects filtered by the ma_nv column
 * @method     ChildNhansu[]|ObjectCollection findByHo(string $ho) Return ChildNhansu objects filtered by the ho column
 * @method     ChildNhansu[]|ObjectCollection findByTen(string $ten) Return ChildNhansu objects filtered by the ten column
 * @method     ChildNhansu[]|ObjectCollection findByGioitinh(string $gioitinh) Return ChildNhansu objects filtered by the gioitinh column
 * @method     ChildNhansu[]|ObjectCollection findByNgaysinh(string $ngaysinh) Return ChildNhansu objects filtered by the ngaysinh column
 * @method     ChildNhansu[]|ObjectCollection findByQuoctich(string $quoctich) Return ChildNhansu objects filtered by the quoctich column
 * @method     ChildNhansu[]|ObjectCollection findByDantoc(string $dantoc) Return ChildNhansu objects filtered by the dantoc column
 * @method     ChildNhansu[]|ObjectCollection findByTongiao(string $tongiao) Return ChildNhansu objects filtered by the tongiao column
 * @method     ChildNhansu[]|ObjectCollection findBySoCmnd(string $so_cmnd) Return ChildNhansu objects filtered by the so_cmnd column
 * @method     ChildNhansu[]|ObjectCollection findByNgaycap(string $ngaycap) Return ChildNhansu objects filtered by the ngaycap column
 * @method     ChildNhansu[]|ObjectCollection findByNoicap(string $noicap) Return ChildNhansu objects filtered by the noicap column
 * @method     ChildNhansu[]|ObjectCollection findByAnh(string $anh) Return ChildNhansu objects filtered by the anh column
 * @method     ChildNhansu[]|ObjectCollection findByDidong(string $didong) Return ChildNhansu objects filtered by the didong column
 * @method     ChildNhansu[]|ObjectCollection findByEmail(string $email) Return ChildNhansu objects filtered by the email column
 * @method     ChildNhansu[]|ObjectCollection findBySkype(string $skype) Return ChildNhansu objects filtered by the skype column
 * @method     ChildNhansu[]|ObjectCollection findByFacebook(string $facebook) Return ChildNhansu objects filtered by the facebook column
 * @method     ChildNhansu[]|ObjectCollection findByQuocgia(string $quocgia) Return ChildNhansu objects filtered by the quocgia column
 * @method     ChildNhansu[]|ObjectCollection findByTinhthanh(string $tinhthanh) Return ChildNhansu objects filtered by the tinhthanh column
 * @method     ChildNhansu[]|ObjectCollection findByQuanhuyen(string $quanhuyen) Return ChildNhansu objects filtered by the quanhuyen column
 * @method     ChildNhansu[]|ObjectCollection findByPhuongxa(string $phuongxa) Return ChildNhansu objects filtered by the phuongxa column
 * @method     ChildNhansu[]|ObjectCollection findByTtkhac(string $ttkhac) Return ChildNhansu objects filtered by the ttkhac column
 * @method     ChildNhansu[]|ObjectCollection findByHonnhan(string $honnhan) Return ChildNhansu objects filtered by the honnhan column
 * @method     ChildNhansu[]|ObjectCollection findByChieucao(string $chieucao) Return ChildNhansu objects filtered by the chieucao column
 * @method     ChildNhansu[]|ObjectCollection findByNhommau(string $nhommau) Return ChildNhansu objects filtered by the nhommau column
 * @method     ChildNhansu[]|ObjectCollection findByXuatthan(string $xuatthan) Return ChildNhansu objects filtered by the xuatthan column
 * @method     ChildNhansu[]|ObjectCollection findBySothich(string $sothich) Return ChildNhansu objects filtered by the sothich column
 * @method     ChildNhansu[]|ObjectCollection findByTrinhdo(string $trinhdo) Return ChildNhansu objects filtered by the trinhdo column
 * @method     ChildNhansu[]|ObjectCollection findByNgoaingu(string $ngoaingu) Return ChildNhansu objects filtered by the ngoaingu column
 * @method     ChildNhansu[]|ObjectCollection findByDaotao(string $daotao) Return ChildNhansu objects filtered by the daotao column
 * @method     ChildNhansu[]|ObjectCollection findByNganh(string $nganh) Return ChildNhansu objects filtered by the nganh column
 * @method     ChildNhansu[]|ObjectCollection findByMoiquanhe(string $moiquanhe) Return ChildNhansu objects filtered by the moiquanhe column
 * @method     ChildNhansu[]|ObjectCollection findByHoten(string $hoten) Return ChildNhansu objects filtered by the hoten column
 * @method     ChildNhansu[]|ObjectCollection findByDiachi(string $diachi) Return ChildNhansu objects filtered by the diachi column
 * @method     ChildNhansu[]|ObjectCollection findByHovaten(string $hovaten) Return ChildNhansu objects filtered by the hovaten column
 * @method     ChildNhansu[]|ObjectCollection findByState(int $state) Return ChildNhansu objects filtered by the state column
 * @method     ChildNhansu[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class NhansuQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \AppBundle\Model\Base\NhansuQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\AppBundle\\Model\\Nhansu', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildNhansuQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildNhansuQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildNhansuQuery) {
            return $criteria;
        }
        $query = new ChildNhansuQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildNhansu|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(NhansuTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = NhansuTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildNhansu A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT `id`, `ma_nv`, `ho`, `ten`, `gioitinh`, `ngaysinh`, `quoctich`, `dantoc`, `tongiao`, `so_cmnd`, `ngaycap`, `noicap`, `anh`, `didong`, `email`, `skype`, `facebook`, `quocgia`, `tinhthanh`, `quanhuyen`, `phuongxa`, `ttkhac`, `honnhan`, `chieucao`, `nhommau`, `xuatthan`, `sothich`, `trinhdo`, `ngoaingu`, `daotao`, `nganh`, `moiquanhe`, `hoten`, `diachi`, `hovaten`, `state` FROM `nhansu` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildNhansu $obj */
            $obj = new ChildNhansu();
            $obj->hydrate($row);
            NhansuTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildNhansu|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildNhansuQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(NhansuTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildNhansuQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(NhansuTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildNhansuQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(NhansuTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(NhansuTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(NhansuTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the ma_nv column
     *
     * Example usage:
     * <code>
     * $query->filterByMaNv('fooValue');   // WHERE ma_nv = 'fooValue'
     * $query->filterByMaNv('%fooValue%', Criteria::LIKE); // WHERE ma_nv LIKE '%fooValue%'
     * </code>
     *
     * @param     string $maNv The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildNhansuQuery The current query, for fluid interface
     */
    public function filterByMaNv($maNv = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($maNv)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(NhansuTableMap::COL_MA_NV, $maNv, $comparison);
    }

    /**
     * Filter the query on the ho column
     *
     * Example usage:
     * <code>
     * $query->filterByHo('fooValue');   // WHERE ho = 'fooValue'
     * $query->filterByHo('%fooValue%', Criteria::LIKE); // WHERE ho LIKE '%fooValue%'
     * </code>
     *
     * @param     string $ho The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildNhansuQuery The current query, for fluid interface
     */
    public function filterByHo($ho = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($ho)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(NhansuTableMap::COL_HO, $ho, $comparison);
    }

    /**
     * Filter the query on the ten column
     *
     * Example usage:
     * <code>
     * $query->filterByTen('fooValue');   // WHERE ten = 'fooValue'
     * $query->filterByTen('%fooValue%', Criteria::LIKE); // WHERE ten LIKE '%fooValue%'
     * </code>
     *
     * @param     string $ten The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildNhansuQuery The current query, for fluid interface
     */
    public function filterByTen($ten = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($ten)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(NhansuTableMap::COL_TEN, $ten, $comparison);
    }

    /**
     * Filter the query on the gioitinh column
     *
     * Example usage:
     * <code>
     * $query->filterByGioitinh('fooValue');   // WHERE gioitinh = 'fooValue'
     * $query->filterByGioitinh('%fooValue%', Criteria::LIKE); // WHERE gioitinh LIKE '%fooValue%'
     * </code>
     *
     * @param     string $gioitinh The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildNhansuQuery The current query, for fluid interface
     */
    public function filterByGioitinh($gioitinh = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($gioitinh)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(NhansuTableMap::COL_GIOITINH, $gioitinh, $comparison);
    }

    /**
     * Filter the query on the ngaysinh column
     *
     * Example usage:
     * <code>
     * $query->filterByNgaysinh('2011-03-14'); // WHERE ngaysinh = '2011-03-14'
     * $query->filterByNgaysinh('now'); // WHERE ngaysinh = '2011-03-14'
     * $query->filterByNgaysinh(array('max' => 'yesterday')); // WHERE ngaysinh > '2011-03-13'
     * </code>
     *
     * @param     mixed $ngaysinh The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildNhansuQuery The current query, for fluid interface
     */
    public function filterByNgaysinh($ngaysinh = null, $comparison = null)
    {
        if (is_array($ngaysinh)) {
            $useMinMax = false;
            if (isset($ngaysinh['min'])) {
                $this->addUsingAlias(NhansuTableMap::COL_NGAYSINH, $ngaysinh['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($ngaysinh['max'])) {
                $this->addUsingAlias(NhansuTableMap::COL_NGAYSINH, $ngaysinh['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(NhansuTableMap::COL_NGAYSINH, $ngaysinh, $comparison);
    }

    /**
     * Filter the query on the quoctich column
     *
     * Example usage:
     * <code>
     * $query->filterByQuoctich('fooValue');   // WHERE quoctich = 'fooValue'
     * $query->filterByQuoctich('%fooValue%', Criteria::LIKE); // WHERE quoctich LIKE '%fooValue%'
     * </code>
     *
     * @param     string $quoctich The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildNhansuQuery The current query, for fluid interface
     */
    public function filterByQuoctich($quoctich = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($quoctich)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(NhansuTableMap::COL_QUOCTICH, $quoctich, $comparison);
    }

    /**
     * Filter the query on the dantoc column
     *
     * Example usage:
     * <code>
     * $query->filterByDantoc('fooValue');   // WHERE dantoc = 'fooValue'
     * $query->filterByDantoc('%fooValue%', Criteria::LIKE); // WHERE dantoc LIKE '%fooValue%'
     * </code>
     *
     * @param     string $dantoc The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildNhansuQuery The current query, for fluid interface
     */
    public function filterByDantoc($dantoc = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($dantoc)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(NhansuTableMap::COL_DANTOC, $dantoc, $comparison);
    }

    /**
     * Filter the query on the tongiao column
     *
     * Example usage:
     * <code>
     * $query->filterByTongiao('fooValue');   // WHERE tongiao = 'fooValue'
     * $query->filterByTongiao('%fooValue%', Criteria::LIKE); // WHERE tongiao LIKE '%fooValue%'
     * </code>
     *
     * @param     string $tongiao The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildNhansuQuery The current query, for fluid interface
     */
    public function filterByTongiao($tongiao = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($tongiao)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(NhansuTableMap::COL_TONGIAO, $tongiao, $comparison);
    }

    /**
     * Filter the query on the so_cmnd column
     *
     * Example usage:
     * <code>
     * $query->filterBySoCmnd('fooValue');   // WHERE so_cmnd = 'fooValue'
     * $query->filterBySoCmnd('%fooValue%', Criteria::LIKE); // WHERE so_cmnd LIKE '%fooValue%'
     * </code>
     *
     * @param     string $soCmnd The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildNhansuQuery The current query, for fluid interface
     */
    public function filterBySoCmnd($soCmnd = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($soCmnd)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(NhansuTableMap::COL_SO_CMND, $soCmnd, $comparison);
    }

    /**
     * Filter the query on the ngaycap column
     *
     * Example usage:
     * <code>
     * $query->filterByNgaycap('2011-03-14'); // WHERE ngaycap = '2011-03-14'
     * $query->filterByNgaycap('now'); // WHERE ngaycap = '2011-03-14'
     * $query->filterByNgaycap(array('max' => 'yesterday')); // WHERE ngaycap > '2011-03-13'
     * </code>
     *
     * @param     mixed $ngaycap The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildNhansuQuery The current query, for fluid interface
     */
    public function filterByNgaycap($ngaycap = null, $comparison = null)
    {
        if (is_array($ngaycap)) {
            $useMinMax = false;
            if (isset($ngaycap['min'])) {
                $this->addUsingAlias(NhansuTableMap::COL_NGAYCAP, $ngaycap['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($ngaycap['max'])) {
                $this->addUsingAlias(NhansuTableMap::COL_NGAYCAP, $ngaycap['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(NhansuTableMap::COL_NGAYCAP, $ngaycap, $comparison);
    }

    /**
     * Filter the query on the noicap column
     *
     * Example usage:
     * <code>
     * $query->filterByNoicap('fooValue');   // WHERE noicap = 'fooValue'
     * $query->filterByNoicap('%fooValue%', Criteria::LIKE); // WHERE noicap LIKE '%fooValue%'
     * </code>
     *
     * @param     string $noicap The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildNhansuQuery The current query, for fluid interface
     */
    public function filterByNoicap($noicap = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($noicap)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(NhansuTableMap::COL_NOICAP, $noicap, $comparison);
    }

    /**
     * Filter the query on the anh column
     *
     * Example usage:
     * <code>
     * $query->filterByAnh('fooValue');   // WHERE anh = 'fooValue'
     * $query->filterByAnh('%fooValue%', Criteria::LIKE); // WHERE anh LIKE '%fooValue%'
     * </code>
     *
     * @param     string $anh The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildNhansuQuery The current query, for fluid interface
     */
    public function filterByAnh($anh = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($anh)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(NhansuTableMap::COL_ANH, $anh, $comparison);
    }

    /**
     * Filter the query on the didong column
     *
     * Example usage:
     * <code>
     * $query->filterByDidong('fooValue');   // WHERE didong = 'fooValue'
     * $query->filterByDidong('%fooValue%', Criteria::LIKE); // WHERE didong LIKE '%fooValue%'
     * </code>
     *
     * @param     string $didong The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildNhansuQuery The current query, for fluid interface
     */
    public function filterByDidong($didong = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($didong)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(NhansuTableMap::COL_DIDONG, $didong, $comparison);
    }

    /**
     * Filter the query on the email column
     *
     * Example usage:
     * <code>
     * $query->filterByEmail('fooValue');   // WHERE email = 'fooValue'
     * $query->filterByEmail('%fooValue%', Criteria::LIKE); // WHERE email LIKE '%fooValue%'
     * </code>
     *
     * @param     string $email The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildNhansuQuery The current query, for fluid interface
     */
    public function filterByEmail($email = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($email)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(NhansuTableMap::COL_EMAIL, $email, $comparison);
    }

    /**
     * Filter the query on the skype column
     *
     * Example usage:
     * <code>
     * $query->filterBySkype('fooValue');   // WHERE skype = 'fooValue'
     * $query->filterBySkype('%fooValue%', Criteria::LIKE); // WHERE skype LIKE '%fooValue%'
     * </code>
     *
     * @param     string $skype The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildNhansuQuery The current query, for fluid interface
     */
    public function filterBySkype($skype = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($skype)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(NhansuTableMap::COL_SKYPE, $skype, $comparison);
    }

    /**
     * Filter the query on the facebook column
     *
     * Example usage:
     * <code>
     * $query->filterByFacebook('fooValue');   // WHERE facebook = 'fooValue'
     * $query->filterByFacebook('%fooValue%', Criteria::LIKE); // WHERE facebook LIKE '%fooValue%'
     * </code>
     *
     * @param     string $facebook The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildNhansuQuery The current query, for fluid interface
     */
    public function filterByFacebook($facebook = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($facebook)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(NhansuTableMap::COL_FACEBOOK, $facebook, $comparison);
    }

    /**
     * Filter the query on the quocgia column
     *
     * Example usage:
     * <code>
     * $query->filterByQuocgia('fooValue');   // WHERE quocgia = 'fooValue'
     * $query->filterByQuocgia('%fooValue%', Criteria::LIKE); // WHERE quocgia LIKE '%fooValue%'
     * </code>
     *
     * @param     string $quocgia The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildNhansuQuery The current query, for fluid interface
     */
    public function filterByQuocgia($quocgia = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($quocgia)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(NhansuTableMap::COL_QUOCGIA, $quocgia, $comparison);
    }

    /**
     * Filter the query on the tinhthanh column
     *
     * Example usage:
     * <code>
     * $query->filterByTinhthanh('fooValue');   // WHERE tinhthanh = 'fooValue'
     * $query->filterByTinhthanh('%fooValue%', Criteria::LIKE); // WHERE tinhthanh LIKE '%fooValue%'
     * </code>
     *
     * @param     string $tinhthanh The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildNhansuQuery The current query, for fluid interface
     */
    public function filterByTinhthanh($tinhthanh = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($tinhthanh)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(NhansuTableMap::COL_TINHTHANH, $tinhthanh, $comparison);
    }

    /**
     * Filter the query on the quanhuyen column
     *
     * Example usage:
     * <code>
     * $query->filterByQuanhuyen('fooValue');   // WHERE quanhuyen = 'fooValue'
     * $query->filterByQuanhuyen('%fooValue%', Criteria::LIKE); // WHERE quanhuyen LIKE '%fooValue%'
     * </code>
     *
     * @param     string $quanhuyen The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildNhansuQuery The current query, for fluid interface
     */
    public function filterByQuanhuyen($quanhuyen = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($quanhuyen)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(NhansuTableMap::COL_QUANHUYEN, $quanhuyen, $comparison);
    }

    /**
     * Filter the query on the phuongxa column
     *
     * Example usage:
     * <code>
     * $query->filterByPhuongxa('fooValue');   // WHERE phuongxa = 'fooValue'
     * $query->filterByPhuongxa('%fooValue%', Criteria::LIKE); // WHERE phuongxa LIKE '%fooValue%'
     * </code>
     *
     * @param     string $phuongxa The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildNhansuQuery The current query, for fluid interface
     */
    public function filterByPhuongxa($phuongxa = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($phuongxa)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(NhansuTableMap::COL_PHUONGXA, $phuongxa, $comparison);
    }

    /**
     * Filter the query on the ttkhac column
     *
     * Example usage:
     * <code>
     * $query->filterByTtkhac('fooValue');   // WHERE ttkhac = 'fooValue'
     * $query->filterByTtkhac('%fooValue%', Criteria::LIKE); // WHERE ttkhac LIKE '%fooValue%'
     * </code>
     *
     * @param     string $ttkhac The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildNhansuQuery The current query, for fluid interface
     */
    public function filterByTtkhac($ttkhac = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($ttkhac)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(NhansuTableMap::COL_TTKHAC, $ttkhac, $comparison);
    }

    /**
     * Filter the query on the honnhan column
     *
     * Example usage:
     * <code>
     * $query->filterByHonnhan('fooValue');   // WHERE honnhan = 'fooValue'
     * $query->filterByHonnhan('%fooValue%', Criteria::LIKE); // WHERE honnhan LIKE '%fooValue%'
     * </code>
     *
     * @param     string $honnhan The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildNhansuQuery The current query, for fluid interface
     */
    public function filterByHonnhan($honnhan = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($honnhan)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(NhansuTableMap::COL_HONNHAN, $honnhan, $comparison);
    }

    /**
     * Filter the query on the chieucao column
     *
     * Example usage:
     * <code>
     * $query->filterByChieucao('fooValue');   // WHERE chieucao = 'fooValue'
     * $query->filterByChieucao('%fooValue%', Criteria::LIKE); // WHERE chieucao LIKE '%fooValue%'
     * </code>
     *
     * @param     string $chieucao The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildNhansuQuery The current query, for fluid interface
     */
    public function filterByChieucao($chieucao = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($chieucao)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(NhansuTableMap::COL_CHIEUCAO, $chieucao, $comparison);
    }

    /**
     * Filter the query on the nhommau column
     *
     * Example usage:
     * <code>
     * $query->filterByNhommau('fooValue');   // WHERE nhommau = 'fooValue'
     * $query->filterByNhommau('%fooValue%', Criteria::LIKE); // WHERE nhommau LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nhommau The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildNhansuQuery The current query, for fluid interface
     */
    public function filterByNhommau($nhommau = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nhommau)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(NhansuTableMap::COL_NHOMMAU, $nhommau, $comparison);
    }

    /**
     * Filter the query on the xuatthan column
     *
     * Example usage:
     * <code>
     * $query->filterByXuatthan('fooValue');   // WHERE xuatthan = 'fooValue'
     * $query->filterByXuatthan('%fooValue%', Criteria::LIKE); // WHERE xuatthan LIKE '%fooValue%'
     * </code>
     *
     * @param     string $xuatthan The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildNhansuQuery The current query, for fluid interface
     */
    public function filterByXuatthan($xuatthan = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($xuatthan)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(NhansuTableMap::COL_XUATTHAN, $xuatthan, $comparison);
    }

    /**
     * Filter the query on the sothich column
     *
     * Example usage:
     * <code>
     * $query->filterBySothich('fooValue');   // WHERE sothich = 'fooValue'
     * $query->filterBySothich('%fooValue%', Criteria::LIKE); // WHERE sothich LIKE '%fooValue%'
     * </code>
     *
     * @param     string $sothich The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildNhansuQuery The current query, for fluid interface
     */
    public function filterBySothich($sothich = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($sothich)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(NhansuTableMap::COL_SOTHICH, $sothich, $comparison);
    }

    /**
     * Filter the query on the trinhdo column
     *
     * Example usage:
     * <code>
     * $query->filterByTrinhdo('fooValue');   // WHERE trinhdo = 'fooValue'
     * $query->filterByTrinhdo('%fooValue%', Criteria::LIKE); // WHERE trinhdo LIKE '%fooValue%'
     * </code>
     *
     * @param     string $trinhdo The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildNhansuQuery The current query, for fluid interface
     */
    public function filterByTrinhdo($trinhdo = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($trinhdo)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(NhansuTableMap::COL_TRINHDO, $trinhdo, $comparison);
    }

    /**
     * Filter the query on the ngoaingu column
     *
     * Example usage:
     * <code>
     * $query->filterByNgoaingu('fooValue');   // WHERE ngoaingu = 'fooValue'
     * $query->filterByNgoaingu('%fooValue%', Criteria::LIKE); // WHERE ngoaingu LIKE '%fooValue%'
     * </code>
     *
     * @param     string $ngoaingu The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildNhansuQuery The current query, for fluid interface
     */
    public function filterByNgoaingu($ngoaingu = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($ngoaingu)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(NhansuTableMap::COL_NGOAINGU, $ngoaingu, $comparison);
    }

    /**
     * Filter the query on the daotao column
     *
     * Example usage:
     * <code>
     * $query->filterByDaotao('fooValue');   // WHERE daotao = 'fooValue'
     * $query->filterByDaotao('%fooValue%', Criteria::LIKE); // WHERE daotao LIKE '%fooValue%'
     * </code>
     *
     * @param     string $daotao The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildNhansuQuery The current query, for fluid interface
     */
    public function filterByDaotao($daotao = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($daotao)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(NhansuTableMap::COL_DAOTAO, $daotao, $comparison);
    }

    /**
     * Filter the query on the nganh column
     *
     * Example usage:
     * <code>
     * $query->filterByNganh('fooValue');   // WHERE nganh = 'fooValue'
     * $query->filterByNganh('%fooValue%', Criteria::LIKE); // WHERE nganh LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nganh The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildNhansuQuery The current query, for fluid interface
     */
    public function filterByNganh($nganh = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nganh)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(NhansuTableMap::COL_NGANH, $nganh, $comparison);
    }

    /**
     * Filter the query on the moiquanhe column
     *
     * Example usage:
     * <code>
     * $query->filterByMoiquanhe('fooValue');   // WHERE moiquanhe = 'fooValue'
     * $query->filterByMoiquanhe('%fooValue%', Criteria::LIKE); // WHERE moiquanhe LIKE '%fooValue%'
     * </code>
     *
     * @param     string $moiquanhe The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildNhansuQuery The current query, for fluid interface
     */
    public function filterByMoiquanhe($moiquanhe = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($moiquanhe)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(NhansuTableMap::COL_MOIQUANHE, $moiquanhe, $comparison);
    }

    /**
     * Filter the query on the hoten column
     *
     * Example usage:
     * <code>
     * $query->filterByHoten('fooValue');   // WHERE hoten = 'fooValue'
     * $query->filterByHoten('%fooValue%', Criteria::LIKE); // WHERE hoten LIKE '%fooValue%'
     * </code>
     *
     * @param     string $hoten The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildNhansuQuery The current query, for fluid interface
     */
    public function filterByHoten($hoten = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($hoten)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(NhansuTableMap::COL_HOTEN, $hoten, $comparison);
    }

    /**
     * Filter the query on the diachi column
     *
     * Example usage:
     * <code>
     * $query->filterByDiachi('fooValue');   // WHERE diachi = 'fooValue'
     * $query->filterByDiachi('%fooValue%', Criteria::LIKE); // WHERE diachi LIKE '%fooValue%'
     * </code>
     *
     * @param     string $diachi The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildNhansuQuery The current query, for fluid interface
     */
    public function filterByDiachi($diachi = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($diachi)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(NhansuTableMap::COL_DIACHI, $diachi, $comparison);
    }

    /**
     * Filter the query on the hovaten column
     *
     * Example usage:
     * <code>
     * $query->filterByHovaten('fooValue');   // WHERE hovaten = 'fooValue'
     * $query->filterByHovaten('%fooValue%', Criteria::LIKE); // WHERE hovaten LIKE '%fooValue%'
     * </code>
     *
     * @param     string $hovaten The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildNhansuQuery The current query, for fluid interface
     */
    public function filterByHovaten($hovaten = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($hovaten)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(NhansuTableMap::COL_HOVATEN, $hovaten, $comparison);
    }

    /**
     * Filter the query on the state column
     *
     * Example usage:
     * <code>
     * $query->filterByState(1234); // WHERE state = 1234
     * $query->filterByState(array(12, 34)); // WHERE state IN (12, 34)
     * $query->filterByState(array('min' => 12)); // WHERE state > 12
     * </code>
     *
     * @param     mixed $state The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildNhansuQuery The current query, for fluid interface
     */
    public function filterByState($state = null, $comparison = null)
    {
        if (is_array($state)) {
            $useMinMax = false;
            if (isset($state['min'])) {
                $this->addUsingAlias(NhansuTableMap::COL_STATE, $state['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($state['max'])) {
                $this->addUsingAlias(NhansuTableMap::COL_STATE, $state['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(NhansuTableMap::COL_STATE, $state, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ChildNhansu $nhansu Object to remove from the list of results
     *
     * @return $this|ChildNhansuQuery The current query, for fluid interface
     */
    public function prune($nhansu = null)
    {
        if ($nhansu) {
            $this->addUsingAlias(NhansuTableMap::COL_ID, $nhansu->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the nhansu table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(NhansuTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            NhansuTableMap::clearInstancePool();
            NhansuTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(NhansuTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(NhansuTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            NhansuTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            NhansuTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // NhansuQuery
