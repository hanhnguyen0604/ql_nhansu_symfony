<?php

namespace AppBundle\Model\Base;

use \DateTime;
use \Exception;
use \PDO;
use AppBundle\Model\NhansuQuery as ChildNhansuQuery;
use AppBundle\Model\Map\NhansuTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'nhansu' table.
 *
 *
 *
 * @package    propel.generator.src\AppBundle.Model.Base
 */
abstract class Nhansu implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\AppBundle\\Model\\Map\\NhansuTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the id field.
     *
     * @var        int
     */
    protected $id;

    /**
     * The value for the ma_nv field.
     *
     * @var        string
     */
    protected $ma_nv;

    /**
     * The value for the ho field.
     *
     * @var        string
     */
    protected $ho;

    /**
     * The value for the ten field.
     *
     * @var        string
     */
    protected $ten;

    /**
     * The value for the gioitinh field.
     *
     * @var        string
     */
    protected $gioitinh;

    /**
     * The value for the ngaysinh field.
     *
     * @var        DateTime
     */
    protected $ngaysinh;

    /**
     * The value for the quoctich field.
     *
     * @var        string
     */
    protected $quoctich;

    /**
     * The value for the dantoc field.
     *
     * @var        string
     */
    protected $dantoc;

    /**
     * The value for the tongiao field.
     *
     * @var        string
     */
    protected $tongiao;

    /**
     * The value for the so_cmnd field.
     *
     * @var        string
     */
    protected $so_cmnd;

    /**
     * The value for the ngaycap field.
     *
     * @var        DateTime
     */
    protected $ngaycap;

    /**
     * The value for the noicap field.
     *
     * @var        string
     */
    protected $noicap;

    /**
     * The value for the anh field.
     *
     * @var        string
     */
    protected $anh;

    /**
     * The value for the didong field.
     *
     * @var        string
     */
    protected $didong;

    /**
     * The value for the email field.
     *
     * @var        string
     */
    protected $email;

    /**
     * The value for the skype field.
     *
     * @var        string
     */
    protected $skype;

    /**
     * The value for the facebook field.
     *
     * @var        string
     */
    protected $facebook;

    /**
     * The value for the quocgia field.
     *
     * @var        string
     */
    protected $quocgia;

    /**
     * The value for the tinhthanh field.
     *
     * @var        string
     */
    protected $tinhthanh;

    /**
     * The value for the quanhuyen field.
     *
     * @var        string
     */
    protected $quanhuyen;

    /**
     * The value for the phuongxa field.
     *
     * @var        string
     */
    protected $phuongxa;

    /**
     * The value for the ttkhac field.
     *
     * @var        string
     */
    protected $ttkhac;

    /**
     * The value for the honnhan field.
     *
     * @var        string
     */
    protected $honnhan;

    /**
     * The value for the chieucao field.
     *
     * @var        string
     */
    protected $chieucao;

    /**
     * The value for the nhommau field.
     *
     * @var        string
     */
    protected $nhommau;

    /**
     * The value for the xuatthan field.
     *
     * @var        string
     */
    protected $xuatthan;

    /**
     * The value for the sothich field.
     *
     * @var        string
     */
    protected $sothich;

    /**
     * The value for the trinhdo field.
     *
     * @var        string
     */
    protected $trinhdo;

    /**
     * The value for the ngoaingu field.
     *
     * @var        string
     */
    protected $ngoaingu;

    /**
     * The value for the daotao field.
     *
     * @var        string
     */
    protected $daotao;

    /**
     * The value for the nganh field.
     *
     * @var        string
     */
    protected $nganh;

    /**
     * The value for the moiquanhe field.
     *
     * @var        string
     */
    protected $moiquanhe;

    /**
     * The value for the hoten field.
     *
     * @var        string
     */
    protected $hoten;

    /**
     * The value for the diachi field.
     *
     * @var        string
     */
    protected $diachi;

    /**
     * The value for the hovaten field.
     *
     * @var        string
     */
    protected $hovaten;

    /**
     * The value for the state field.
     *
     * @var        int
     */
    protected $state;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * Initializes internal state of AppBundle\Model\Base\Nhansu object.
     */
    public function __construct()
    {
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>Nhansu</code> instance.  If
     * <code>obj</code> is an instance of <code>Nhansu</code>, delegates to
     * <code>equals(Nhansu)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|Nhansu The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the [ma_nv] column value.
     *
     * @return string
     */
    public function getMaNv()
    {
        return $this->ma_nv;
    }

    /**
     * Get the [ho] column value.
     *
     * @return string
     */
    public function getHo()
    {
        return $this->ho;
    }

    /**
     * Get the [ten] column value.
     *
     * @return string
     */
    public function getTen()
    {
        return $this->ten;
    }

    /**
     * Get the [gioitinh] column value.
     *
     * @return string
     */
    public function getGioitinh()
    {
        return $this->gioitinh;
    }

    /**
     * Get the [optionally formatted] temporal [ngaysinh] column value.
     *
     *
     * @param      string|null $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getNgaysinh($format = NULL)
    {
        if ($format === null) {
            return $this->ngaysinh;
        } else {
            return $this->ngaysinh instanceof \DateTimeInterface ? $this->ngaysinh->format($format) : null;
        }
    }

    /**
     * Get the [quoctich] column value.
     *
     * @return string
     */
    public function getQuoctich()
    {
        return $this->quoctich;
    }

    /**
     * Get the [dantoc] column value.
     *
     * @return string
     */
    public function getDantoc()
    {
        return $this->dantoc;
    }

    /**
     * Get the [tongiao] column value.
     *
     * @return string
     */
    public function getTongiao()
    {
        return $this->tongiao;
    }

    /**
     * Get the [so_cmnd] column value.
     *
     * @return string
     */
    public function getSoCmnd()
    {
        return $this->so_cmnd;
    }

    /**
     * Get the [optionally formatted] temporal [ngaycap] column value.
     *
     *
     * @param      string|null $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getNgaycap($format = NULL)
    {
        if ($format === null) {
            return $this->ngaycap;
        } else {
            return $this->ngaycap instanceof \DateTimeInterface ? $this->ngaycap->format($format) : null;
        }
    }

    /**
     * Get the [noicap] column value.
     *
     * @return string
     */
    public function getNoicap()
    {
        return $this->noicap;
    }

    /**
     * Get the [anh] column value.
     *
     * @return string
     */
    public function getAnh()
    {
        return $this->anh;
    }

    /**
     * Get the [didong] column value.
     *
     * @return string
     */
    public function getDidong()
    {
        return $this->didong;
    }

    /**
     * Get the [email] column value.
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Get the [skype] column value.
     *
     * @return string
     */
    public function getSkype()
    {
        return $this->skype;
    }

    /**
     * Get the [facebook] column value.
     *
     * @return string
     */
    public function getFacebook()
    {
        return $this->facebook;
    }

    /**
     * Get the [quocgia] column value.
     *
     * @return string
     */
    public function getQuocgia()
    {
        return $this->quocgia;
    }

    /**
     * Get the [tinhthanh] column value.
     *
     * @return string
     */
    public function getTinhthanh()
    {
        return $this->tinhthanh;
    }

    /**
     * Get the [quanhuyen] column value.
     *
     * @return string
     */
    public function getQuanhuyen()
    {
        return $this->quanhuyen;
    }

    /**
     * Get the [phuongxa] column value.
     *
     * @return string
     */
    public function getPhuongxa()
    {
        return $this->phuongxa;
    }

    /**
     * Get the [ttkhac] column value.
     *
     * @return string
     */
    public function getTtkhac()
    {
        return $this->ttkhac;
    }

    /**
     * Get the [honnhan] column value.
     *
     * @return string
     */
    public function getHonnhan()
    {
        return $this->honnhan;
    }

    /**
     * Get the [chieucao] column value.
     *
     * @return string
     */
    public function getChieucao()
    {
        return $this->chieucao;
    }

    /**
     * Get the [nhommau] column value.
     *
     * @return string
     */
    public function getNhommau()
    {
        return $this->nhommau;
    }

    /**
     * Get the [xuatthan] column value.
     *
     * @return string
     */
    public function getXuatthan()
    {
        return $this->xuatthan;
    }

    /**
     * Get the [sothich] column value.
     *
     * @return string
     */
    public function getSothich()
    {
        return $this->sothich;
    }

    /**
     * Get the [trinhdo] column value.
     *
     * @return string
     */
    public function getTrinhdo()
    {
        return $this->trinhdo;
    }

    /**
     * Get the [ngoaingu] column value.
     *
     * @return string
     */
    public function getNgoaingu()
    {
        return $this->ngoaingu;
    }

    /**
     * Get the [daotao] column value.
     *
     * @return string
     */
    public function getDaotao()
    {
        return $this->daotao;
    }

    /**
     * Get the [nganh] column value.
     *
     * @return string
     */
    public function getNganh()
    {
        return $this->nganh;
    }

    /**
     * Get the [moiquanhe] column value.
     *
     * @return string
     */
    public function getMoiquanhe()
    {
        return $this->moiquanhe;
    }

    /**
     * Get the [hoten] column value.
     *
     * @return string
     */
    public function getHoten()
    {
        return $this->hoten;
    }

    /**
     * Get the [diachi] column value.
     *
     * @return string
     */
    public function getDiachi()
    {
        return $this->diachi;
    }

    /**
     * Get the [hovaten] column value.
     *
     * @return string
     */
    public function getHovaten()
    {
        return $this->hovaten;
    }

    /**
     * Get the [state] column value.
     *
     * @return int
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Set the value of [id] column.
     *
     * @param int $v new value
     * @return $this|\AppBundle\Model\Nhansu The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[NhansuTableMap::COL_ID] = true;
        }

        return $this;
    } // setId()

    /**
     * Set the value of [ma_nv] column.
     *
     * @param string $v new value
     * @return $this|\AppBundle\Model\Nhansu The current object (for fluent API support)
     */
    public function setMaNv($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->ma_nv !== $v) {
            $this->ma_nv = $v;
            $this->modifiedColumns[NhansuTableMap::COL_MA_NV] = true;
        }

        return $this;
    } // setMaNv()

    /**
     * Set the value of [ho] column.
     *
     * @param string $v new value
     * @return $this|\AppBundle\Model\Nhansu The current object (for fluent API support)
     */
    public function setHo($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->ho !== $v) {
            $this->ho = $v;
            $this->modifiedColumns[NhansuTableMap::COL_HO] = true;
        }

        return $this;
    } // setHo()

    /**
     * Set the value of [ten] column.
     *
     * @param string $v new value
     * @return $this|\AppBundle\Model\Nhansu The current object (for fluent API support)
     */
    public function setTen($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->ten !== $v) {
            $this->ten = $v;
            $this->modifiedColumns[NhansuTableMap::COL_TEN] = true;
        }

        return $this;
    } // setTen()

    /**
     * Set the value of [gioitinh] column.
     *
     * @param string $v new value
     * @return $this|\AppBundle\Model\Nhansu The current object (for fluent API support)
     */
    public function setGioitinh($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->gioitinh !== $v) {
            $this->gioitinh = $v;
            $this->modifiedColumns[NhansuTableMap::COL_GIOITINH] = true;
        }

        return $this;
    } // setGioitinh()

    /**
     * Sets the value of [ngaysinh] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\AppBundle\Model\Nhansu The current object (for fluent API support)
     */
    public function setNgaysinh($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->ngaysinh !== null || $dt !== null) {
            if ($this->ngaysinh === null || $dt === null || $dt->format("Y-m-d") !== $this->ngaysinh->format("Y-m-d")) {
                $this->ngaysinh = $dt === null ? null : clone $dt;
                $this->modifiedColumns[NhansuTableMap::COL_NGAYSINH] = true;
            }
        } // if either are not null

        return $this;
    } // setNgaysinh()

    /**
     * Set the value of [quoctich] column.
     *
     * @param string $v new value
     * @return $this|\AppBundle\Model\Nhansu The current object (for fluent API support)
     */
    public function setQuoctich($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->quoctich !== $v) {
            $this->quoctich = $v;
            $this->modifiedColumns[NhansuTableMap::COL_QUOCTICH] = true;
        }

        return $this;
    } // setQuoctich()

    /**
     * Set the value of [dantoc] column.
     *
     * @param string $v new value
     * @return $this|\AppBundle\Model\Nhansu The current object (for fluent API support)
     */
    public function setDantoc($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->dantoc !== $v) {
            $this->dantoc = $v;
            $this->modifiedColumns[NhansuTableMap::COL_DANTOC] = true;
        }

        return $this;
    } // setDantoc()

    /**
     * Set the value of [tongiao] column.
     *
     * @param string $v new value
     * @return $this|\AppBundle\Model\Nhansu The current object (for fluent API support)
     */
    public function setTongiao($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->tongiao !== $v) {
            $this->tongiao = $v;
            $this->modifiedColumns[NhansuTableMap::COL_TONGIAO] = true;
        }

        return $this;
    } // setTongiao()

    /**
     * Set the value of [so_cmnd] column.
     *
     * @param string $v new value
     * @return $this|\AppBundle\Model\Nhansu The current object (for fluent API support)
     */
    public function setSoCmnd($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->so_cmnd !== $v) {
            $this->so_cmnd = $v;
            $this->modifiedColumns[NhansuTableMap::COL_SO_CMND] = true;
        }

        return $this;
    } // setSoCmnd()

    /**
     * Sets the value of [ngaycap] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\AppBundle\Model\Nhansu The current object (for fluent API support)
     */
    public function setNgaycap($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->ngaycap !== null || $dt !== null) {
            if ($this->ngaycap === null || $dt === null || $dt->format("Y-m-d") !== $this->ngaycap->format("Y-m-d")) {
                $this->ngaycap = $dt === null ? null : clone $dt;
                $this->modifiedColumns[NhansuTableMap::COL_NGAYCAP] = true;
            }
        } // if either are not null

        return $this;
    } // setNgaycap()

    /**
     * Set the value of [noicap] column.
     *
     * @param string $v new value
     * @return $this|\AppBundle\Model\Nhansu The current object (for fluent API support)
     */
    public function setNoicap($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->noicap !== $v) {
            $this->noicap = $v;
            $this->modifiedColumns[NhansuTableMap::COL_NOICAP] = true;
        }

        return $this;
    } // setNoicap()

    /**
     * Set the value of [anh] column.
     *
     * @param string $v new value
     * @return $this|\AppBundle\Model\Nhansu The current object (for fluent API support)
     */
    public function setAnh($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->anh !== $v) {
            $this->anh = $v;
            $this->modifiedColumns[NhansuTableMap::COL_ANH] = true;
        }

        return $this;
    } // setAnh()

    /**
     * Set the value of [didong] column.
     *
     * @param string $v new value
     * @return $this|\AppBundle\Model\Nhansu The current object (for fluent API support)
     */
    public function setDidong($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->didong !== $v) {
            $this->didong = $v;
            $this->modifiedColumns[NhansuTableMap::COL_DIDONG] = true;
        }

        return $this;
    } // setDidong()

    /**
     * Set the value of [email] column.
     *
     * @param string $v new value
     * @return $this|\AppBundle\Model\Nhansu The current object (for fluent API support)
     */
    public function setEmail($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->email !== $v) {
            $this->email = $v;
            $this->modifiedColumns[NhansuTableMap::COL_EMAIL] = true;
        }

        return $this;
    } // setEmail()

    /**
     * Set the value of [skype] column.
     *
     * @param string $v new value
     * @return $this|\AppBundle\Model\Nhansu The current object (for fluent API support)
     */
    public function setSkype($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->skype !== $v) {
            $this->skype = $v;
            $this->modifiedColumns[NhansuTableMap::COL_SKYPE] = true;
        }

        return $this;
    } // setSkype()

    /**
     * Set the value of [facebook] column.
     *
     * @param string $v new value
     * @return $this|\AppBundle\Model\Nhansu The current object (for fluent API support)
     */
    public function setFacebook($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->facebook !== $v) {
            $this->facebook = $v;
            $this->modifiedColumns[NhansuTableMap::COL_FACEBOOK] = true;
        }

        return $this;
    } // setFacebook()

    /**
     * Set the value of [quocgia] column.
     *
     * @param string $v new value
     * @return $this|\AppBundle\Model\Nhansu The current object (for fluent API support)
     */
    public function setQuocgia($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->quocgia !== $v) {
            $this->quocgia = $v;
            $this->modifiedColumns[NhansuTableMap::COL_QUOCGIA] = true;
        }

        return $this;
    } // setQuocgia()

    /**
     * Set the value of [tinhthanh] column.
     *
     * @param string $v new value
     * @return $this|\AppBundle\Model\Nhansu The current object (for fluent API support)
     */
    public function setTinhthanh($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->tinhthanh !== $v) {
            $this->tinhthanh = $v;
            $this->modifiedColumns[NhansuTableMap::COL_TINHTHANH] = true;
        }

        return $this;
    } // setTinhthanh()

    /**
     * Set the value of [quanhuyen] column.
     *
     * @param string $v new value
     * @return $this|\AppBundle\Model\Nhansu The current object (for fluent API support)
     */
    public function setQuanhuyen($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->quanhuyen !== $v) {
            $this->quanhuyen = $v;
            $this->modifiedColumns[NhansuTableMap::COL_QUANHUYEN] = true;
        }

        return $this;
    } // setQuanhuyen()

    /**
     * Set the value of [phuongxa] column.
     *
     * @param string $v new value
     * @return $this|\AppBundle\Model\Nhansu The current object (for fluent API support)
     */
    public function setPhuongxa($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->phuongxa !== $v) {
            $this->phuongxa = $v;
            $this->modifiedColumns[NhansuTableMap::COL_PHUONGXA] = true;
        }

        return $this;
    } // setPhuongxa()

    /**
     * Set the value of [ttkhac] column.
     *
     * @param string $v new value
     * @return $this|\AppBundle\Model\Nhansu The current object (for fluent API support)
     */
    public function setTtkhac($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->ttkhac !== $v) {
            $this->ttkhac = $v;
            $this->modifiedColumns[NhansuTableMap::COL_TTKHAC] = true;
        }

        return $this;
    } // setTtkhac()

    /**
     * Set the value of [honnhan] column.
     *
     * @param string $v new value
     * @return $this|\AppBundle\Model\Nhansu The current object (for fluent API support)
     */
    public function setHonnhan($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->honnhan !== $v) {
            $this->honnhan = $v;
            $this->modifiedColumns[NhansuTableMap::COL_HONNHAN] = true;
        }

        return $this;
    } // setHonnhan()

    /**
     * Set the value of [chieucao] column.
     *
     * @param string $v new value
     * @return $this|\AppBundle\Model\Nhansu The current object (for fluent API support)
     */
    public function setChieucao($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->chieucao !== $v) {
            $this->chieucao = $v;
            $this->modifiedColumns[NhansuTableMap::COL_CHIEUCAO] = true;
        }

        return $this;
    } // setChieucao()

    /**
     * Set the value of [nhommau] column.
     *
     * @param string $v new value
     * @return $this|\AppBundle\Model\Nhansu The current object (for fluent API support)
     */
    public function setNhommau($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->nhommau !== $v) {
            $this->nhommau = $v;
            $this->modifiedColumns[NhansuTableMap::COL_NHOMMAU] = true;
        }

        return $this;
    } // setNhommau()

    /**
     * Set the value of [xuatthan] column.
     *
     * @param string $v new value
     * @return $this|\AppBundle\Model\Nhansu The current object (for fluent API support)
     */
    public function setXuatthan($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->xuatthan !== $v) {
            $this->xuatthan = $v;
            $this->modifiedColumns[NhansuTableMap::COL_XUATTHAN] = true;
        }

        return $this;
    } // setXuatthan()

    /**
     * Set the value of [sothich] column.
     *
     * @param string $v new value
     * @return $this|\AppBundle\Model\Nhansu The current object (for fluent API support)
     */
    public function setSothich($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->sothich !== $v) {
            $this->sothich = $v;
            $this->modifiedColumns[NhansuTableMap::COL_SOTHICH] = true;
        }

        return $this;
    } // setSothich()

    /**
     * Set the value of [trinhdo] column.
     *
     * @param string $v new value
     * @return $this|\AppBundle\Model\Nhansu The current object (for fluent API support)
     */
    public function setTrinhdo($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->trinhdo !== $v) {
            $this->trinhdo = $v;
            $this->modifiedColumns[NhansuTableMap::COL_TRINHDO] = true;
        }

        return $this;
    } // setTrinhdo()

    /**
     * Set the value of [ngoaingu] column.
     *
     * @param string $v new value
     * @return $this|\AppBundle\Model\Nhansu The current object (for fluent API support)
     */
    public function setNgoaingu($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->ngoaingu !== $v) {
            $this->ngoaingu = $v;
            $this->modifiedColumns[NhansuTableMap::COL_NGOAINGU] = true;
        }

        return $this;
    } // setNgoaingu()

    /**
     * Set the value of [daotao] column.
     *
     * @param string $v new value
     * @return $this|\AppBundle\Model\Nhansu The current object (for fluent API support)
     */
    public function setDaotao($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->daotao !== $v) {
            $this->daotao = $v;
            $this->modifiedColumns[NhansuTableMap::COL_DAOTAO] = true;
        }

        return $this;
    } // setDaotao()

    /**
     * Set the value of [nganh] column.
     *
     * @param string $v new value
     * @return $this|\AppBundle\Model\Nhansu The current object (for fluent API support)
     */
    public function setNganh($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->nganh !== $v) {
            $this->nganh = $v;
            $this->modifiedColumns[NhansuTableMap::COL_NGANH] = true;
        }

        return $this;
    } // setNganh()

    /**
     * Set the value of [moiquanhe] column.
     *
     * @param string $v new value
     * @return $this|\AppBundle\Model\Nhansu The current object (for fluent API support)
     */
    public function setMoiquanhe($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->moiquanhe !== $v) {
            $this->moiquanhe = $v;
            $this->modifiedColumns[NhansuTableMap::COL_MOIQUANHE] = true;
        }

        return $this;
    } // setMoiquanhe()

    /**
     * Set the value of [hoten] column.
     *
     * @param string $v new value
     * @return $this|\AppBundle\Model\Nhansu The current object (for fluent API support)
     */
    public function setHoten($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->hoten !== $v) {
            $this->hoten = $v;
            $this->modifiedColumns[NhansuTableMap::COL_HOTEN] = true;
        }

        return $this;
    } // setHoten()

    /**
     * Set the value of [diachi] column.
     *
     * @param string $v new value
     * @return $this|\AppBundle\Model\Nhansu The current object (for fluent API support)
     */
    public function setDiachi($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->diachi !== $v) {
            $this->diachi = $v;
            $this->modifiedColumns[NhansuTableMap::COL_DIACHI] = true;
        }

        return $this;
    } // setDiachi()

    /**
     * Set the value of [hovaten] column.
     *
     * @param string $v new value
     * @return $this|\AppBundle\Model\Nhansu The current object (for fluent API support)
     */
    public function setHovaten($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->hovaten !== $v) {
            $this->hovaten = $v;
            $this->modifiedColumns[NhansuTableMap::COL_HOVATEN] = true;
        }

        return $this;
    } // setHovaten()

    /**
     * Set the value of [state] column.
     *
     * @param int $v new value
     * @return $this|\AppBundle\Model\Nhansu The current object (for fluent API support)
     */
    public function setState($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->state !== $v) {
            $this->state = $v;
            $this->modifiedColumns[NhansuTableMap::COL_STATE] = true;
        }

        return $this;
    } // setState()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : NhansuTableMap::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : NhansuTableMap::translateFieldName('MaNv', TableMap::TYPE_PHPNAME, $indexType)];
            $this->ma_nv = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : NhansuTableMap::translateFieldName('Ho', TableMap::TYPE_PHPNAME, $indexType)];
            $this->ho = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : NhansuTableMap::translateFieldName('Ten', TableMap::TYPE_PHPNAME, $indexType)];
            $this->ten = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : NhansuTableMap::translateFieldName('Gioitinh', TableMap::TYPE_PHPNAME, $indexType)];
            $this->gioitinh = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : NhansuTableMap::translateFieldName('Ngaysinh', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00') {
                $col = null;
            }
            $this->ngaysinh = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : NhansuTableMap::translateFieldName('Quoctich', TableMap::TYPE_PHPNAME, $indexType)];
            $this->quoctich = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : NhansuTableMap::translateFieldName('Dantoc', TableMap::TYPE_PHPNAME, $indexType)];
            $this->dantoc = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : NhansuTableMap::translateFieldName('Tongiao', TableMap::TYPE_PHPNAME, $indexType)];
            $this->tongiao = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : NhansuTableMap::translateFieldName('SoCmnd', TableMap::TYPE_PHPNAME, $indexType)];
            $this->so_cmnd = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 10 + $startcol : NhansuTableMap::translateFieldName('Ngaycap', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00') {
                $col = null;
            }
            $this->ngaycap = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 11 + $startcol : NhansuTableMap::translateFieldName('Noicap', TableMap::TYPE_PHPNAME, $indexType)];
            $this->noicap = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 12 + $startcol : NhansuTableMap::translateFieldName('Anh', TableMap::TYPE_PHPNAME, $indexType)];
            $this->anh = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 13 + $startcol : NhansuTableMap::translateFieldName('Didong', TableMap::TYPE_PHPNAME, $indexType)];
            $this->didong = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 14 + $startcol : NhansuTableMap::translateFieldName('Email', TableMap::TYPE_PHPNAME, $indexType)];
            $this->email = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 15 + $startcol : NhansuTableMap::translateFieldName('Skype', TableMap::TYPE_PHPNAME, $indexType)];
            $this->skype = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 16 + $startcol : NhansuTableMap::translateFieldName('Facebook', TableMap::TYPE_PHPNAME, $indexType)];
            $this->facebook = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 17 + $startcol : NhansuTableMap::translateFieldName('Quocgia', TableMap::TYPE_PHPNAME, $indexType)];
            $this->quocgia = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 18 + $startcol : NhansuTableMap::translateFieldName('Tinhthanh', TableMap::TYPE_PHPNAME, $indexType)];
            $this->tinhthanh = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 19 + $startcol : NhansuTableMap::translateFieldName('Quanhuyen', TableMap::TYPE_PHPNAME, $indexType)];
            $this->quanhuyen = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 20 + $startcol : NhansuTableMap::translateFieldName('Phuongxa', TableMap::TYPE_PHPNAME, $indexType)];
            $this->phuongxa = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 21 + $startcol : NhansuTableMap::translateFieldName('Ttkhac', TableMap::TYPE_PHPNAME, $indexType)];
            $this->ttkhac = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 22 + $startcol : NhansuTableMap::translateFieldName('Honnhan', TableMap::TYPE_PHPNAME, $indexType)];
            $this->honnhan = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 23 + $startcol : NhansuTableMap::translateFieldName('Chieucao', TableMap::TYPE_PHPNAME, $indexType)];
            $this->chieucao = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 24 + $startcol : NhansuTableMap::translateFieldName('Nhommau', TableMap::TYPE_PHPNAME, $indexType)];
            $this->nhommau = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 25 + $startcol : NhansuTableMap::translateFieldName('Xuatthan', TableMap::TYPE_PHPNAME, $indexType)];
            $this->xuatthan = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 26 + $startcol : NhansuTableMap::translateFieldName('Sothich', TableMap::TYPE_PHPNAME, $indexType)];
            $this->sothich = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 27 + $startcol : NhansuTableMap::translateFieldName('Trinhdo', TableMap::TYPE_PHPNAME, $indexType)];
            $this->trinhdo = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 28 + $startcol : NhansuTableMap::translateFieldName('Ngoaingu', TableMap::TYPE_PHPNAME, $indexType)];
            $this->ngoaingu = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 29 + $startcol : NhansuTableMap::translateFieldName('Daotao', TableMap::TYPE_PHPNAME, $indexType)];
            $this->daotao = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 30 + $startcol : NhansuTableMap::translateFieldName('Nganh', TableMap::TYPE_PHPNAME, $indexType)];
            $this->nganh = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 31 + $startcol : NhansuTableMap::translateFieldName('Moiquanhe', TableMap::TYPE_PHPNAME, $indexType)];
            $this->moiquanhe = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 32 + $startcol : NhansuTableMap::translateFieldName('Hoten', TableMap::TYPE_PHPNAME, $indexType)];
            $this->hoten = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 33 + $startcol : NhansuTableMap::translateFieldName('Diachi', TableMap::TYPE_PHPNAME, $indexType)];
            $this->diachi = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 34 + $startcol : NhansuTableMap::translateFieldName('Hovaten', TableMap::TYPE_PHPNAME, $indexType)];
            $this->hovaten = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 35 + $startcol : NhansuTableMap::translateFieldName('State', TableMap::TYPE_PHPNAME, $indexType)];
            $this->state = (null !== $col) ? (int) $col : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 36; // 36 = NhansuTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\AppBundle\\Model\\Nhansu'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(NhansuTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildNhansuQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see Nhansu::setDeleted()
     * @see Nhansu::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(NhansuTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildNhansuQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(NhansuTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                NhansuTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[NhansuTableMap::COL_ID] = true;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . NhansuTableMap::COL_ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(NhansuTableMap::COL_ID)) {
            $modifiedColumns[':p' . $index++]  = '`id`';
        }
        if ($this->isColumnModified(NhansuTableMap::COL_MA_NV)) {
            $modifiedColumns[':p' . $index++]  = '`ma_nv`';
        }
        if ($this->isColumnModified(NhansuTableMap::COL_HO)) {
            $modifiedColumns[':p' . $index++]  = '`ho`';
        }
        if ($this->isColumnModified(NhansuTableMap::COL_TEN)) {
            $modifiedColumns[':p' . $index++]  = '`ten`';
        }
        if ($this->isColumnModified(NhansuTableMap::COL_GIOITINH)) {
            $modifiedColumns[':p' . $index++]  = '`gioitinh`';
        }
        if ($this->isColumnModified(NhansuTableMap::COL_NGAYSINH)) {
            $modifiedColumns[':p' . $index++]  = '`ngaysinh`';
        }
        if ($this->isColumnModified(NhansuTableMap::COL_QUOCTICH)) {
            $modifiedColumns[':p' . $index++]  = '`quoctich`';
        }
        if ($this->isColumnModified(NhansuTableMap::COL_DANTOC)) {
            $modifiedColumns[':p' . $index++]  = '`dantoc`';
        }
        if ($this->isColumnModified(NhansuTableMap::COL_TONGIAO)) {
            $modifiedColumns[':p' . $index++]  = '`tongiao`';
        }
        if ($this->isColumnModified(NhansuTableMap::COL_SO_CMND)) {
            $modifiedColumns[':p' . $index++]  = '`so_cmnd`';
        }
        if ($this->isColumnModified(NhansuTableMap::COL_NGAYCAP)) {
            $modifiedColumns[':p' . $index++]  = '`ngaycap`';
        }
        if ($this->isColumnModified(NhansuTableMap::COL_NOICAP)) {
            $modifiedColumns[':p' . $index++]  = '`noicap`';
        }
        if ($this->isColumnModified(NhansuTableMap::COL_ANH)) {
            $modifiedColumns[':p' . $index++]  = '`anh`';
        }
        if ($this->isColumnModified(NhansuTableMap::COL_DIDONG)) {
            $modifiedColumns[':p' . $index++]  = '`didong`';
        }
        if ($this->isColumnModified(NhansuTableMap::COL_EMAIL)) {
            $modifiedColumns[':p' . $index++]  = '`email`';
        }
        if ($this->isColumnModified(NhansuTableMap::COL_SKYPE)) {
            $modifiedColumns[':p' . $index++]  = '`skype`';
        }
        if ($this->isColumnModified(NhansuTableMap::COL_FACEBOOK)) {
            $modifiedColumns[':p' . $index++]  = '`facebook`';
        }
        if ($this->isColumnModified(NhansuTableMap::COL_QUOCGIA)) {
            $modifiedColumns[':p' . $index++]  = '`quocgia`';
        }
        if ($this->isColumnModified(NhansuTableMap::COL_TINHTHANH)) {
            $modifiedColumns[':p' . $index++]  = '`tinhthanh`';
        }
        if ($this->isColumnModified(NhansuTableMap::COL_QUANHUYEN)) {
            $modifiedColumns[':p' . $index++]  = '`quanhuyen`';
        }
        if ($this->isColumnModified(NhansuTableMap::COL_PHUONGXA)) {
            $modifiedColumns[':p' . $index++]  = '`phuongxa`';
        }
        if ($this->isColumnModified(NhansuTableMap::COL_TTKHAC)) {
            $modifiedColumns[':p' . $index++]  = '`ttkhac`';
        }
        if ($this->isColumnModified(NhansuTableMap::COL_HONNHAN)) {
            $modifiedColumns[':p' . $index++]  = '`honnhan`';
        }
        if ($this->isColumnModified(NhansuTableMap::COL_CHIEUCAO)) {
            $modifiedColumns[':p' . $index++]  = '`chieucao`';
        }
        if ($this->isColumnModified(NhansuTableMap::COL_NHOMMAU)) {
            $modifiedColumns[':p' . $index++]  = '`nhommau`';
        }
        if ($this->isColumnModified(NhansuTableMap::COL_XUATTHAN)) {
            $modifiedColumns[':p' . $index++]  = '`xuatthan`';
        }
        if ($this->isColumnModified(NhansuTableMap::COL_SOTHICH)) {
            $modifiedColumns[':p' . $index++]  = '`sothich`';
        }
        if ($this->isColumnModified(NhansuTableMap::COL_TRINHDO)) {
            $modifiedColumns[':p' . $index++]  = '`trinhdo`';
        }
        if ($this->isColumnModified(NhansuTableMap::COL_NGOAINGU)) {
            $modifiedColumns[':p' . $index++]  = '`ngoaingu`';
        }
        if ($this->isColumnModified(NhansuTableMap::COL_DAOTAO)) {
            $modifiedColumns[':p' . $index++]  = '`daotao`';
        }
        if ($this->isColumnModified(NhansuTableMap::COL_NGANH)) {
            $modifiedColumns[':p' . $index++]  = '`nganh`';
        }
        if ($this->isColumnModified(NhansuTableMap::COL_MOIQUANHE)) {
            $modifiedColumns[':p' . $index++]  = '`moiquanhe`';
        }
        if ($this->isColumnModified(NhansuTableMap::COL_HOTEN)) {
            $modifiedColumns[':p' . $index++]  = '`hoten`';
        }
        if ($this->isColumnModified(NhansuTableMap::COL_DIACHI)) {
            $modifiedColumns[':p' . $index++]  = '`diachi`';
        }
        if ($this->isColumnModified(NhansuTableMap::COL_HOVATEN)) {
            $modifiedColumns[':p' . $index++]  = '`hovaten`';
        }
        if ($this->isColumnModified(NhansuTableMap::COL_STATE)) {
            $modifiedColumns[':p' . $index++]  = '`state`';
        }

        $sql = sprintf(
            'INSERT INTO `nhansu` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`id`':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case '`ma_nv`':
                        $stmt->bindValue($identifier, $this->ma_nv, PDO::PARAM_STR);
                        break;
                    case '`ho`':
                        $stmt->bindValue($identifier, $this->ho, PDO::PARAM_STR);
                        break;
                    case '`ten`':
                        $stmt->bindValue($identifier, $this->ten, PDO::PARAM_STR);
                        break;
                    case '`gioitinh`':
                        $stmt->bindValue($identifier, $this->gioitinh, PDO::PARAM_STR);
                        break;
                    case '`ngaysinh`':
                        $stmt->bindValue($identifier, $this->ngaysinh ? $this->ngaysinh->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case '`quoctich`':
                        $stmt->bindValue($identifier, $this->quoctich, PDO::PARAM_STR);
                        break;
                    case '`dantoc`':
                        $stmt->bindValue($identifier, $this->dantoc, PDO::PARAM_STR);
                        break;
                    case '`tongiao`':
                        $stmt->bindValue($identifier, $this->tongiao, PDO::PARAM_STR);
                        break;
                    case '`so_cmnd`':
                        $stmt->bindValue($identifier, $this->so_cmnd, PDO::PARAM_STR);
                        break;
                    case '`ngaycap`':
                        $stmt->bindValue($identifier, $this->ngaycap ? $this->ngaycap->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case '`noicap`':
                        $stmt->bindValue($identifier, $this->noicap, PDO::PARAM_STR);
                        break;
                    case '`anh`':
                        $stmt->bindValue($identifier, $this->anh, PDO::PARAM_STR);
                        break;
                    case '`didong`':
                        $stmt->bindValue($identifier, $this->didong, PDO::PARAM_STR);
                        break;
                    case '`email`':
                        $stmt->bindValue($identifier, $this->email, PDO::PARAM_STR);
                        break;
                    case '`skype`':
                        $stmt->bindValue($identifier, $this->skype, PDO::PARAM_STR);
                        break;
                    case '`facebook`':
                        $stmt->bindValue($identifier, $this->facebook, PDO::PARAM_STR);
                        break;
                    case '`quocgia`':
                        $stmt->bindValue($identifier, $this->quocgia, PDO::PARAM_STR);
                        break;
                    case '`tinhthanh`':
                        $stmt->bindValue($identifier, $this->tinhthanh, PDO::PARAM_STR);
                        break;
                    case '`quanhuyen`':
                        $stmt->bindValue($identifier, $this->quanhuyen, PDO::PARAM_STR);
                        break;
                    case '`phuongxa`':
                        $stmt->bindValue($identifier, $this->phuongxa, PDO::PARAM_STR);
                        break;
                    case '`ttkhac`':
                        $stmt->bindValue($identifier, $this->ttkhac, PDO::PARAM_STR);
                        break;
                    case '`honnhan`':
                        $stmt->bindValue($identifier, $this->honnhan, PDO::PARAM_STR);
                        break;
                    case '`chieucao`':
                        $stmt->bindValue($identifier, $this->chieucao, PDO::PARAM_STR);
                        break;
                    case '`nhommau`':
                        $stmt->bindValue($identifier, $this->nhommau, PDO::PARAM_STR);
                        break;
                    case '`xuatthan`':
                        $stmt->bindValue($identifier, $this->xuatthan, PDO::PARAM_STR);
                        break;
                    case '`sothich`':
                        $stmt->bindValue($identifier, $this->sothich, PDO::PARAM_STR);
                        break;
                    case '`trinhdo`':
                        $stmt->bindValue($identifier, $this->trinhdo, PDO::PARAM_STR);
                        break;
                    case '`ngoaingu`':
                        $stmt->bindValue($identifier, $this->ngoaingu, PDO::PARAM_STR);
                        break;
                    case '`daotao`':
                        $stmt->bindValue($identifier, $this->daotao, PDO::PARAM_STR);
                        break;
                    case '`nganh`':
                        $stmt->bindValue($identifier, $this->nganh, PDO::PARAM_STR);
                        break;
                    case '`moiquanhe`':
                        $stmt->bindValue($identifier, $this->moiquanhe, PDO::PARAM_STR);
                        break;
                    case '`hoten`':
                        $stmt->bindValue($identifier, $this->hoten, PDO::PARAM_STR);
                        break;
                    case '`diachi`':
                        $stmt->bindValue($identifier, $this->diachi, PDO::PARAM_STR);
                        break;
                    case '`hovaten`':
                        $stmt->bindValue($identifier, $this->hovaten, PDO::PARAM_STR);
                        break;
                    case '`state`':
                        $stmt->bindValue($identifier, $this->state, PDO::PARAM_INT);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = NhansuTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getMaNv();
                break;
            case 2:
                return $this->getHo();
                break;
            case 3:
                return $this->getTen();
                break;
            case 4:
                return $this->getGioitinh();
                break;
            case 5:
                return $this->getNgaysinh();
                break;
            case 6:
                return $this->getQuoctich();
                break;
            case 7:
                return $this->getDantoc();
                break;
            case 8:
                return $this->getTongiao();
                break;
            case 9:
                return $this->getSoCmnd();
                break;
            case 10:
                return $this->getNgaycap();
                break;
            case 11:
                return $this->getNoicap();
                break;
            case 12:
                return $this->getAnh();
                break;
            case 13:
                return $this->getDidong();
                break;
            case 14:
                return $this->getEmail();
                break;
            case 15:
                return $this->getSkype();
                break;
            case 16:
                return $this->getFacebook();
                break;
            case 17:
                return $this->getQuocgia();
                break;
            case 18:
                return $this->getTinhthanh();
                break;
            case 19:
                return $this->getQuanhuyen();
                break;
            case 20:
                return $this->getPhuongxa();
                break;
            case 21:
                return $this->getTtkhac();
                break;
            case 22:
                return $this->getHonnhan();
                break;
            case 23:
                return $this->getChieucao();
                break;
            case 24:
                return $this->getNhommau();
                break;
            case 25:
                return $this->getXuatthan();
                break;
            case 26:
                return $this->getSothich();
                break;
            case 27:
                return $this->getTrinhdo();
                break;
            case 28:
                return $this->getNgoaingu();
                break;
            case 29:
                return $this->getDaotao();
                break;
            case 30:
                return $this->getNganh();
                break;
            case 31:
                return $this->getMoiquanhe();
                break;
            case 32:
                return $this->getHoten();
                break;
            case 33:
                return $this->getDiachi();
                break;
            case 34:
                return $this->getHovaten();
                break;
            case 35:
                return $this->getState();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array())
    {

        if (isset($alreadyDumpedObjects['Nhansu'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Nhansu'][$this->hashCode()] = true;
        $keys = NhansuTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getMaNv(),
            $keys[2] => $this->getHo(),
            $keys[3] => $this->getTen(),
            $keys[4] => $this->getGioitinh(),
            $keys[5] => $this->getNgaysinh(),
            $keys[6] => $this->getQuoctich(),
            $keys[7] => $this->getDantoc(),
            $keys[8] => $this->getTongiao(),
            $keys[9] => $this->getSoCmnd(),
            $keys[10] => $this->getNgaycap(),
            $keys[11] => $this->getNoicap(),
            $keys[12] => $this->getAnh(),
            $keys[13] => $this->getDidong(),
            $keys[14] => $this->getEmail(),
            $keys[15] => $this->getSkype(),
            $keys[16] => $this->getFacebook(),
            $keys[17] => $this->getQuocgia(),
            $keys[18] => $this->getTinhthanh(),
            $keys[19] => $this->getQuanhuyen(),
            $keys[20] => $this->getPhuongxa(),
            $keys[21] => $this->getTtkhac(),
            $keys[22] => $this->getHonnhan(),
            $keys[23] => $this->getChieucao(),
            $keys[24] => $this->getNhommau(),
            $keys[25] => $this->getXuatthan(),
            $keys[26] => $this->getSothich(),
            $keys[27] => $this->getTrinhdo(),
            $keys[28] => $this->getNgoaingu(),
            $keys[29] => $this->getDaotao(),
            $keys[30] => $this->getNganh(),
            $keys[31] => $this->getMoiquanhe(),
            $keys[32] => $this->getHoten(),
            $keys[33] => $this->getDiachi(),
            $keys[34] => $this->getHovaten(),
            $keys[35] => $this->getState(),
        );
        if ($result[$keys[5]] instanceof \DateTimeInterface) {
            $result[$keys[5]] = $result[$keys[5]]->format('c');
        }

        if ($result[$keys[10]] instanceof \DateTimeInterface) {
            $result[$keys[10]] = $result[$keys[10]]->format('c');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }


        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\AppBundle\Model\Nhansu
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = NhansuTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\AppBundle\Model\Nhansu
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setMaNv($value);
                break;
            case 2:
                $this->setHo($value);
                break;
            case 3:
                $this->setTen($value);
                break;
            case 4:
                $this->setGioitinh($value);
                break;
            case 5:
                $this->setNgaysinh($value);
                break;
            case 6:
                $this->setQuoctich($value);
                break;
            case 7:
                $this->setDantoc($value);
                break;
            case 8:
                $this->setTongiao($value);
                break;
            case 9:
                $this->setSoCmnd($value);
                break;
            case 10:
                $this->setNgaycap($value);
                break;
            case 11:
                $this->setNoicap($value);
                break;
            case 12:
                $this->setAnh($value);
                break;
            case 13:
                $this->setDidong($value);
                break;
            case 14:
                $this->setEmail($value);
                break;
            case 15:
                $this->setSkype($value);
                break;
            case 16:
                $this->setFacebook($value);
                break;
            case 17:
                $this->setQuocgia($value);
                break;
            case 18:
                $this->setTinhthanh($value);
                break;
            case 19:
                $this->setQuanhuyen($value);
                break;
            case 20:
                $this->setPhuongxa($value);
                break;
            case 21:
                $this->setTtkhac($value);
                break;
            case 22:
                $this->setHonnhan($value);
                break;
            case 23:
                $this->setChieucao($value);
                break;
            case 24:
                $this->setNhommau($value);
                break;
            case 25:
                $this->setXuatthan($value);
                break;
            case 26:
                $this->setSothich($value);
                break;
            case 27:
                $this->setTrinhdo($value);
                break;
            case 28:
                $this->setNgoaingu($value);
                break;
            case 29:
                $this->setDaotao($value);
                break;
            case 30:
                $this->setNganh($value);
                break;
            case 31:
                $this->setMoiquanhe($value);
                break;
            case 32:
                $this->setHoten($value);
                break;
            case 33:
                $this->setDiachi($value);
                break;
            case 34:
                $this->setHovaten($value);
                break;
            case 35:
                $this->setState($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = NhansuTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setMaNv($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setHo($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setTen($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setGioitinh($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setNgaysinh($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setQuoctich($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setDantoc($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setTongiao($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setSoCmnd($arr[$keys[9]]);
        }
        if (array_key_exists($keys[10], $arr)) {
            $this->setNgaycap($arr[$keys[10]]);
        }
        if (array_key_exists($keys[11], $arr)) {
            $this->setNoicap($arr[$keys[11]]);
        }
        if (array_key_exists($keys[12], $arr)) {
            $this->setAnh($arr[$keys[12]]);
        }
        if (array_key_exists($keys[13], $arr)) {
            $this->setDidong($arr[$keys[13]]);
        }
        if (array_key_exists($keys[14], $arr)) {
            $this->setEmail($arr[$keys[14]]);
        }
        if (array_key_exists($keys[15], $arr)) {
            $this->setSkype($arr[$keys[15]]);
        }
        if (array_key_exists($keys[16], $arr)) {
            $this->setFacebook($arr[$keys[16]]);
        }
        if (array_key_exists($keys[17], $arr)) {
            $this->setQuocgia($arr[$keys[17]]);
        }
        if (array_key_exists($keys[18], $arr)) {
            $this->setTinhthanh($arr[$keys[18]]);
        }
        if (array_key_exists($keys[19], $arr)) {
            $this->setQuanhuyen($arr[$keys[19]]);
        }
        if (array_key_exists($keys[20], $arr)) {
            $this->setPhuongxa($arr[$keys[20]]);
        }
        if (array_key_exists($keys[21], $arr)) {
            $this->setTtkhac($arr[$keys[21]]);
        }
        if (array_key_exists($keys[22], $arr)) {
            $this->setHonnhan($arr[$keys[22]]);
        }
        if (array_key_exists($keys[23], $arr)) {
            $this->setChieucao($arr[$keys[23]]);
        }
        if (array_key_exists($keys[24], $arr)) {
            $this->setNhommau($arr[$keys[24]]);
        }
        if (array_key_exists($keys[25], $arr)) {
            $this->setXuatthan($arr[$keys[25]]);
        }
        if (array_key_exists($keys[26], $arr)) {
            $this->setSothich($arr[$keys[26]]);
        }
        if (array_key_exists($keys[27], $arr)) {
            $this->setTrinhdo($arr[$keys[27]]);
        }
        if (array_key_exists($keys[28], $arr)) {
            $this->setNgoaingu($arr[$keys[28]]);
        }
        if (array_key_exists($keys[29], $arr)) {
            $this->setDaotao($arr[$keys[29]]);
        }
        if (array_key_exists($keys[30], $arr)) {
            $this->setNganh($arr[$keys[30]]);
        }
        if (array_key_exists($keys[31], $arr)) {
            $this->setMoiquanhe($arr[$keys[31]]);
        }
        if (array_key_exists($keys[32], $arr)) {
            $this->setHoten($arr[$keys[32]]);
        }
        if (array_key_exists($keys[33], $arr)) {
            $this->setDiachi($arr[$keys[33]]);
        }
        if (array_key_exists($keys[34], $arr)) {
            $this->setHovaten($arr[$keys[34]]);
        }
        if (array_key_exists($keys[35], $arr)) {
            $this->setState($arr[$keys[35]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\AppBundle\Model\Nhansu The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(NhansuTableMap::DATABASE_NAME);

        if ($this->isColumnModified(NhansuTableMap::COL_ID)) {
            $criteria->add(NhansuTableMap::COL_ID, $this->id);
        }
        if ($this->isColumnModified(NhansuTableMap::COL_MA_NV)) {
            $criteria->add(NhansuTableMap::COL_MA_NV, $this->ma_nv);
        }
        if ($this->isColumnModified(NhansuTableMap::COL_HO)) {
            $criteria->add(NhansuTableMap::COL_HO, $this->ho);
        }
        if ($this->isColumnModified(NhansuTableMap::COL_TEN)) {
            $criteria->add(NhansuTableMap::COL_TEN, $this->ten);
        }
        if ($this->isColumnModified(NhansuTableMap::COL_GIOITINH)) {
            $criteria->add(NhansuTableMap::COL_GIOITINH, $this->gioitinh);
        }
        if ($this->isColumnModified(NhansuTableMap::COL_NGAYSINH)) {
            $criteria->add(NhansuTableMap::COL_NGAYSINH, $this->ngaysinh);
        }
        if ($this->isColumnModified(NhansuTableMap::COL_QUOCTICH)) {
            $criteria->add(NhansuTableMap::COL_QUOCTICH, $this->quoctich);
        }
        if ($this->isColumnModified(NhansuTableMap::COL_DANTOC)) {
            $criteria->add(NhansuTableMap::COL_DANTOC, $this->dantoc);
        }
        if ($this->isColumnModified(NhansuTableMap::COL_TONGIAO)) {
            $criteria->add(NhansuTableMap::COL_TONGIAO, $this->tongiao);
        }
        if ($this->isColumnModified(NhansuTableMap::COL_SO_CMND)) {
            $criteria->add(NhansuTableMap::COL_SO_CMND, $this->so_cmnd);
        }
        if ($this->isColumnModified(NhansuTableMap::COL_NGAYCAP)) {
            $criteria->add(NhansuTableMap::COL_NGAYCAP, $this->ngaycap);
        }
        if ($this->isColumnModified(NhansuTableMap::COL_NOICAP)) {
            $criteria->add(NhansuTableMap::COL_NOICAP, $this->noicap);
        }
        if ($this->isColumnModified(NhansuTableMap::COL_ANH)) {
            $criteria->add(NhansuTableMap::COL_ANH, $this->anh);
        }
        if ($this->isColumnModified(NhansuTableMap::COL_DIDONG)) {
            $criteria->add(NhansuTableMap::COL_DIDONG, $this->didong);
        }
        if ($this->isColumnModified(NhansuTableMap::COL_EMAIL)) {
            $criteria->add(NhansuTableMap::COL_EMAIL, $this->email);
        }
        if ($this->isColumnModified(NhansuTableMap::COL_SKYPE)) {
            $criteria->add(NhansuTableMap::COL_SKYPE, $this->skype);
        }
        if ($this->isColumnModified(NhansuTableMap::COL_FACEBOOK)) {
            $criteria->add(NhansuTableMap::COL_FACEBOOK, $this->facebook);
        }
        if ($this->isColumnModified(NhansuTableMap::COL_QUOCGIA)) {
            $criteria->add(NhansuTableMap::COL_QUOCGIA, $this->quocgia);
        }
        if ($this->isColumnModified(NhansuTableMap::COL_TINHTHANH)) {
            $criteria->add(NhansuTableMap::COL_TINHTHANH, $this->tinhthanh);
        }
        if ($this->isColumnModified(NhansuTableMap::COL_QUANHUYEN)) {
            $criteria->add(NhansuTableMap::COL_QUANHUYEN, $this->quanhuyen);
        }
        if ($this->isColumnModified(NhansuTableMap::COL_PHUONGXA)) {
            $criteria->add(NhansuTableMap::COL_PHUONGXA, $this->phuongxa);
        }
        if ($this->isColumnModified(NhansuTableMap::COL_TTKHAC)) {
            $criteria->add(NhansuTableMap::COL_TTKHAC, $this->ttkhac);
        }
        if ($this->isColumnModified(NhansuTableMap::COL_HONNHAN)) {
            $criteria->add(NhansuTableMap::COL_HONNHAN, $this->honnhan);
        }
        if ($this->isColumnModified(NhansuTableMap::COL_CHIEUCAO)) {
            $criteria->add(NhansuTableMap::COL_CHIEUCAO, $this->chieucao);
        }
        if ($this->isColumnModified(NhansuTableMap::COL_NHOMMAU)) {
            $criteria->add(NhansuTableMap::COL_NHOMMAU, $this->nhommau);
        }
        if ($this->isColumnModified(NhansuTableMap::COL_XUATTHAN)) {
            $criteria->add(NhansuTableMap::COL_XUATTHAN, $this->xuatthan);
        }
        if ($this->isColumnModified(NhansuTableMap::COL_SOTHICH)) {
            $criteria->add(NhansuTableMap::COL_SOTHICH, $this->sothich);
        }
        if ($this->isColumnModified(NhansuTableMap::COL_TRINHDO)) {
            $criteria->add(NhansuTableMap::COL_TRINHDO, $this->trinhdo);
        }
        if ($this->isColumnModified(NhansuTableMap::COL_NGOAINGU)) {
            $criteria->add(NhansuTableMap::COL_NGOAINGU, $this->ngoaingu);
        }
        if ($this->isColumnModified(NhansuTableMap::COL_DAOTAO)) {
            $criteria->add(NhansuTableMap::COL_DAOTAO, $this->daotao);
        }
        if ($this->isColumnModified(NhansuTableMap::COL_NGANH)) {
            $criteria->add(NhansuTableMap::COL_NGANH, $this->nganh);
        }
        if ($this->isColumnModified(NhansuTableMap::COL_MOIQUANHE)) {
            $criteria->add(NhansuTableMap::COL_MOIQUANHE, $this->moiquanhe);
        }
        if ($this->isColumnModified(NhansuTableMap::COL_HOTEN)) {
            $criteria->add(NhansuTableMap::COL_HOTEN, $this->hoten);
        }
        if ($this->isColumnModified(NhansuTableMap::COL_DIACHI)) {
            $criteria->add(NhansuTableMap::COL_DIACHI, $this->diachi);
        }
        if ($this->isColumnModified(NhansuTableMap::COL_HOVATEN)) {
            $criteria->add(NhansuTableMap::COL_HOVATEN, $this->hovaten);
        }
        if ($this->isColumnModified(NhansuTableMap::COL_STATE)) {
            $criteria->add(NhansuTableMap::COL_STATE, $this->state);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildNhansuQuery::create();
        $criteria->add(NhansuTableMap::COL_ID, $this->id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \AppBundle\Model\Nhansu (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setMaNv($this->getMaNv());
        $copyObj->setHo($this->getHo());
        $copyObj->setTen($this->getTen());
        $copyObj->setGioitinh($this->getGioitinh());
        $copyObj->setNgaysinh($this->getNgaysinh());
        $copyObj->setQuoctich($this->getQuoctich());
        $copyObj->setDantoc($this->getDantoc());
        $copyObj->setTongiao($this->getTongiao());
        $copyObj->setSoCmnd($this->getSoCmnd());
        $copyObj->setNgaycap($this->getNgaycap());
        $copyObj->setNoicap($this->getNoicap());
        $copyObj->setAnh($this->getAnh());
        $copyObj->setDidong($this->getDidong());
        $copyObj->setEmail($this->getEmail());
        $copyObj->setSkype($this->getSkype());
        $copyObj->setFacebook($this->getFacebook());
        $copyObj->setQuocgia($this->getQuocgia());
        $copyObj->setTinhthanh($this->getTinhthanh());
        $copyObj->setQuanhuyen($this->getQuanhuyen());
        $copyObj->setPhuongxa($this->getPhuongxa());
        $copyObj->setTtkhac($this->getTtkhac());
        $copyObj->setHonnhan($this->getHonnhan());
        $copyObj->setChieucao($this->getChieucao());
        $copyObj->setNhommau($this->getNhommau());
        $copyObj->setXuatthan($this->getXuatthan());
        $copyObj->setSothich($this->getSothich());
        $copyObj->setTrinhdo($this->getTrinhdo());
        $copyObj->setNgoaingu($this->getNgoaingu());
        $copyObj->setDaotao($this->getDaotao());
        $copyObj->setNganh($this->getNganh());
        $copyObj->setMoiquanhe($this->getMoiquanhe());
        $copyObj->setHoten($this->getHoten());
        $copyObj->setDiachi($this->getDiachi());
        $copyObj->setHovaten($this->getHovaten());
        $copyObj->setState($this->getState());
        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \AppBundle\Model\Nhansu Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        $this->id = null;
        $this->ma_nv = null;
        $this->ho = null;
        $this->ten = null;
        $this->gioitinh = null;
        $this->ngaysinh = null;
        $this->quoctich = null;
        $this->dantoc = null;
        $this->tongiao = null;
        $this->so_cmnd = null;
        $this->ngaycap = null;
        $this->noicap = null;
        $this->anh = null;
        $this->didong = null;
        $this->email = null;
        $this->skype = null;
        $this->facebook = null;
        $this->quocgia = null;
        $this->tinhthanh = null;
        $this->quanhuyen = null;
        $this->phuongxa = null;
        $this->ttkhac = null;
        $this->honnhan = null;
        $this->chieucao = null;
        $this->nhommau = null;
        $this->xuatthan = null;
        $this->sothich = null;
        $this->trinhdo = null;
        $this->ngoaingu = null;
        $this->daotao = null;
        $this->nganh = null;
        $this->moiquanhe = null;
        $this->hoten = null;
        $this->diachi = null;
        $this->hovaten = null;
        $this->state = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
        } // if ($deep)

    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(NhansuTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
