
# This is a fix for InnoDB in MySQL >= 4.1.x
# It "suspends judgement" for fkey relationships until are tables are set.
SET FOREIGN_KEY_CHECKS = 0;

-- ---------------------------------------------------------------------
-- nhansu
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `nhansu`;

CREATE TABLE `nhansu`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `ma_nv` VARCHAR(11),
    `ho` VARCHAR(30),
    `ten` VARCHAR(30),
    `gioitinh` VARCHAR(30),
    `ngaysinh` DATE,
    `quoctich` VARCHAR(100),
    `dantoc` VARCHAR(50),
    `tongiao` VARCHAR(50),
    `so_cmnd` VARCHAR(50),
    `ngaycap` DATE,
    `noicap` VARCHAR(50),
    `anh` VARCHAR(50),
    `didong` VARCHAR(50),
    `email` VARCHAR(50),
    `skype` VARCHAR(50),
    `facebook` VARCHAR(50),
    `quocgia` VARCHAR(50),
    `tinhthanh` VARCHAR(50),
    `quanhuyen` VARCHAR(50),
    `phuongxa` VARCHAR(50),
    `ttkhac` VARCHAR(200),
    `honnhan` VARCHAR(50),
    `chieucao` VARCHAR(50),
    `nhommau` VARCHAR(50),
    `xuatthan` VARCHAR(100),
    `sothich` VARCHAR(500),
    `trinhdo` VARCHAR(100),
    `ngoaingu` VARCHAR(100),
    `daotao` VARCHAR(100),
    `nganh` VARCHAR(50),
    `moiquanhe` VARCHAR(50),
    `hoten` VARCHAR(30),
    `diachi` VARCHAR(200),
    `state` TINYINT(1),
    PRIMARY KEY (`id`)
) ENGINE=InnoDB CHARACTER SET='utf8' COLLATE='utf8_unicode_ci';

-- ---------------------------------------------------------------------
-- admin
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `admin`;

CREATE TABLE `admin`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `account` VARCHAR(30),
    `password` VARCHAR(20),
    `images` VARCHAR(150),
    `email` VARCHAR(30),
    PRIMARY KEY (`id`)
) ENGINE=InnoDB CHARACTER SET='utf8' COLLATE='utf8_unicode_ci';

# This restores the fkey checks, after having unset them earlier
SET FOREIGN_KEY_CHECKS = 1;
